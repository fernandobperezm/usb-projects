# errores.py
#
# Descripción: Este es el módulo que contiene todas las clases de error del sistema.
#
# Estudiantes:
#   Br. Leslie Rodrigues, carné: 10-10613.
#   Br. Fernando Pérez, carné: 12-11152.
#
# Última modificación: 03/07/2016.

# Context: Se encarga de manejar errores de contexto. Es una clase que hereda
#de Exception. Posee un mensaje que será mostrado al momento del error.
class ContextError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

# DynamicError: Se encarga de manejar errores dinamicos. Es una clase que hereda
# de Exception. Posee un mensaje que será mostrado al momento del error.
class DynamicError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
