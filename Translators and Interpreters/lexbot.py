# lexbot.py
#
# Descripción: Este es el módulo principal del lexer.
#
# Estudiantes:
#   Br. Leslie Rodrigues, carné: 10-10613.
#   Br. Fernando Pérez, carné: 12-11152.
#
# Última modificación: 21/01/2016.
#

import ply.lex as lex

# Lista de los tokens a manipular.
tokens = [
    #Este denota los literales de caracter.
    'TkCaracter',
    
    #Este denotan los identificadores
    'TkIdent',

    #Este denota los literales numéricos.
    'TkNum',
    
    #Estos denotan los símbolos separadores.
    'TkComa',
    'TkPunto',
    'TkDosPuntos',
    'TkParAbre',
    'TkParCierra',
          
    #Estos denotan los símbolos de operadores aritméticos, booleanos,
    #relacionales o de otro tipo en BOT.
    'TkSuma',
    'TkResta',
    'TkMult',
    'TkDiv',
    'TkMod',
    'TkConjuncion',
    'TkDisyuncion',
    'TkNegacion',
    'TkMenor',
    'TkMenorIgual',
    'TkMayor',
    'TkMayorIgual',
    'TkIgual'
]
#Este denotan palabras reservadas del lenguaje
reservadas = {
    r'create': 'TkCreate',
    r'execute': 'TkExecute',
    r'end': 'TkEnd',
    r'bot': 'TkBot',
    r'on':'TkOn',
    r'as': 'TkAs',
    
    #Control de flujo.
    r'if': 'TkIf' ,
    r'else': 'TkElse',
    r'while': 'TkWhile',
    
    #tipos.
    r'char': 'TkChar',
    r'int': 'TkInt' ,
    r'bool': 'TkBool',
    
    #instrucciones del controlador.
    r'activate': 'TkActivate',
    r'advance': 'TkAdvance',
    r'deactivate': 'TkDeactivate',
        
    #instrucciones de robot.
    r'store': 'TkStore',
    r'collect': 'TkCollect',
    r'drop': 'TkDrop' ,
    r'left': 'TkLeft',
    r'right': 'TkRight',
    r'up': 'TkUp',
    r'down': 'TkDown',
    r'read': 'TkRead',
    r'send': 'TkSend',
    
    #condiciones
    r'activation': 'TkActivation',
    r'deactivation': 'TkDeactivation',
    r'default': 'TkDefault',
    
    #Literales booleanos.
    r'true' : 'TkTrue',
	r'false' : 'TkFalse'
}

tokens += list(reservadas.values()) 

#Se define la función del lexer para el lenguaje BOT.
def lexBot():
    # Reglas de expresiones regulares.
    #Reglas para caracteres.
    t_TkCaracter = r"'(.|\\s|\\t)'"

    #Reglas para los separadores.
    t_TkComa    = r','
    t_TkPunto   = r'\.'
    t_TkDosPuntos   = r':'
    t_TkParAbre  = r'\('
    t_TkParCierra  = r'\)'

    #Reglas para los operadores aritméticos, booleanos, relacionales o de otro 
    # tipo en BOT.
    t_TkSuma = r'\+'
    t_TkResta = r'-'
    t_TkMult = r'\*'
    t_TkDiv = r'/'
    t_TkMod = r'%'
    t_TkConjuncion = r'/\\'
    t_TkDisyuncion = r'\\/'
    t_TkNegacion = r'~'
    t_TkMenor = r'<'
    t_TkMenorIgual = r'<='
    t_TkMayor = r'>'
    t_TkMayorIgual = r'>='
    t_TkIgual = r'='

    # Los espacios, tabuladores y comentarios de cola son ignorados por estas
    #reglas.
    t_ignore  = ' \t'
    t_ignore_comentarioDeCola = r'\$\$.*'
	
	# Regla para ignorar bloques de comentarios.
    def t_ignore_comentario(t):
        r'\$-([^-]|(-)+[^\$])*-\$'
        #actualiza el número de la fila.
        t.lexer.lineno += (t.value).count('\n')
        #actualiza la posición donde comienza la linea actual.
        t.lexer.posRelativa += (t.value).rfind('\n') + 1   
        
    # Regla para el manejo de caracteres inválidos.
    def t_error(t):
        columna = t.lexpos - t.lexer.posRelativa + 1
        print('Error: Caracter no válido "' + str(t.value[0]) + 
         '" en la fila ' + str(t.lineno) + ", columna " + str(columna) + ".")
        t.lexer.error = True
        t.lexer.skip(1) 
    
    # Regla para el manejo de identificadores inválidos.   
    def t_error_IDInvalido(t):
        #Si un identificador comienza con dígitos, entonces es inválido.
        r'([0-9]+[a-zA-Z_]+[0-9]*)+'
        t.lexer.error = True
        columna = t.lexpos - t.lexer.posRelativa + 1
        print('Error: Identificador no válido "' + str(t.value) + 
         '" en la fila ' + str(t.lineno) + ", columna " + str(columna) + ".")
        t.lexer.skip(1)
         
    # Regla para el manejo de identificadores y palabras reservadas, así como
    # tipos booleanos.
    def t_TkIdent(t):
        r'[a-zA-Z][a-zA-Z0-9_]*'
        t.type = reservadas.get(t.value,'TkIdent')
        return t
        	
    # Regla de expresión regular para los literales númericos.
    def t_TkNum(t):
        r'\d+'
        #Se almacena en el token, el entero correspondiente a la entrada.
        t.value = int(t.value)
        return t

    #Esta función lleva el conteo del número de líneas visitadas.
    def t_newline(t):
        r'\n+'
        #Actualiza el número de fila.
        t.lexer.lineno += len(t.value)
        #actualiza la posición donde comienza la línea actual.
        t.lexer.posRelativa = t.lexer.lexpos
    

    return lex.lex()
