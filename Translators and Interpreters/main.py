# main.py
#
# Descripción: Ciclo principal del proyecto.
#
# Estudiantes:
#   Br. Leslie Rodrigues, carné: 10-10613.
#   Br. Fernando Pérez, carné: 12-11152.
#
# Última modificación: 02/15/2016.
#

import pdb
from lexbot import *
from sintbot import *
import sys

#Primeramente, se recibe los argumentos de la línea de comandos.
archivo = sys.argv[1]

#En argumentos[1] se almacena el nombre del archivo a leer. Se procede a abrir 
#el archivo y a leer todo su contenido.
with open(archivo,'r') as f:
    data = f.read()
f.closed

#Crear el lexer.
lexer = lexBot()

#Se añaden tres atributos al lexer. Uno para manejar el primer token, el segundo 
#para el manejo de errores y el tercero para el cálculo de la posición relativa 
#al principio de la línea del token actual.
lexer.primero = True
lexer.error = False
lexer.posRelativa = 0

#Se inserta lo leido del archivo en el lexer.
lexer.input(data)

#Se declara la lista de tokens que se imprimirán en pantalla en caso de no haber 
#caracteres inválidos en el archivo. También, se declara una lista que indica la
#columna de cada token.
toks = []
columns = []
columna = 0

#Se procede a tokenizar.
while True:
    #Se obtiene el próximo token.
    tok = lexer.token()
    
    #Si no hay más tokens, entonces, se deja de ciclar.
    if not tok:
        break
        
    #Para cada token se calcula la columna actual.
    columna = tok.lexpos - lexer.posRelativa + 1
 
    #Se almacena el token y su columna actual.
    toks.append(tok) 
    columns.append(columna)
    
#Si no hubo caracteres inválidos en el archivo, se procede a imprimir los tokens.
if not (lexer.error):
    #Se recorre la lista creada anteriormente para la impresión.
    for i in range(len(toks)):
        #Se verifica si se está imprimiendo por primera vez o no.
        if not (lexer.primero):
            print(',', end = ' ')
        else:
            lexer.primero = False
    
        #Se escribe la salida de acuerdo al tipo de token obtenido.
        if ((toks[i].type == 'TkNum') or (toks[i].type == 'TkCaracter')):
            print(toks[i].type+'('+str(toks[i].value)+')' + ' ' +
               str(toks[i].lineno) + ' ' + str(columns[i]),end = '')
            
        elif (toks[i].type == 'TkIdent'):
            print(toks[i].type+'("'+str(toks[i].value)+'")' + ' ' +
               str(toks[i].lineno) + ' ' + str(columns[i]),end = '')
        else:
            print(toks[i].type + ' ' + str(toks[i].lineno) + ' ' +
               str(columns[i]),end = '')
    
    print()
    
    #Si no hay errores léxicos, se procede a analizar sintácticamente el archivo.
    parser = yacc.yacc()
    ast = parser.parse(data)
    ast.analisisContexto()
    arbol = ast.imprimir()
    global matriz
    matriz = Matriz()
    arbolDeclaraciones = [ast]
    ast.recorrer(arbolDeclaraciones)
    print('\n' + arbol)
