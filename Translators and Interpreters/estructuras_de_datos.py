# estructuras_de_datos.py
#
# Descripción: Este es el módulo que contiene todas las estructuras de datos
# implementadas para el desarrollo de la etapa 2.
#
# Estudiantes:
#   Br. Leslie Rodrigues, carné: 10-10613.
#   Br. Fernando Pérez, carné: 12-11152.
#
# Última modificación: 03/07/2016.
#
from errores import *
from interprete import *
import pdb
    
# TablaSimb: representa la tabla de simbolos necesaria para el analisis de contexto 
# simbolos: un diccionario, donde la clave corresponde al nombre de la variable
#           y el valor almacena el tipo asociado a la variable
# padre: referencia al padre de la tabla actual        
class TablaSimb:
    #Constructor.
    def __init__ (self, simbolos = {}, padre = None):
       self.simbolos = simbolos
       self.padre = padre
    
    # anadirSimb: anade simbolo a la tabla
    # simbolo: variable a anadir en la tabla
    # tipo: tipo de la variable
    # retorna: False si el simbolo ya estaba en la tabla, y true en caso contrario
    def anadirSimb(self, simbolo, tipo,valor = None,estatus = False,posx = 0, posy = 0):
        if (simbolo in self.simbolos):
            return False
        self.simbolos[simbolo] = [tipo,valor,estatus,posx,posy]
        return True
    
    # buscarSimb: busca variable en la tabla
    # simbolo: variable a buscar
    # Retorna: True si el simbolo esta en la tabla, y False en caso contrario
    def buscarSimb(self, simbolo):
        aux = self
        while aux != None:
            if (simbolo in aux.simbolos): 
                return True
            aux = aux.padre
        return False
    
    # buscarTipo: busca el tipo de una variable en la tabla
    # simbolo: variable a buscar
    # Retorna: El tipo de la variable cuando la variable esta en la tabla
    #          y None en caso contrario
    def buscarTipo(self, simbolo):
        aux = self
        while aux != None:
            if (simbolo in aux.simbolos):
                return aux.simbolos[simbolo][0]
            aux = aux.padre
        return None
    
    # buscarEstatus: busca el estatus de una variable en la tabla
    # simbolo: variable a buscar
    # Retorna: El estatus de la variable cuando la variable esta en la tabla
    #          y None en caso contrario    
    def buscarEstatus(self,simbolo):
        aux = self
        while aux != None:
            if (simbolo in aux.simbolos):
                return aux.simbolos[simbolo]
            aux = aux.padre
        return None
    
    def buscarTabla(self,simbolo):
        aux = self
        while aux != None:
            if (simbolo in aux.simbolos):
                return aux
            aux = aux.padre
        return None
        
    def actualizarValor(self,simbolo,valor):
        aux = self
        while aux != None:
            if (simbolo in aux.simbolos):
                aux.simbolos[simbolo] = valor
                return True
            aux = aux.padre
        return None
    
class nodo:
    # tipo: almacena una cadena de caracterteres para identificar el contenido del nodo
    # hijos: lista donde se almacenan nodos
    # hoja: es none o una lista que contine cadenas de caracteres,
    #       correspondientes a palabras del lenguaje 
    def __init__ (self, tipo, hijos = None, hoja = None, tabla = None):
       self.tipo = tipo;
       if hijos:
           self.hijos = hijos
       else:
           self.hijos = []
       self.hoja = hoja
       self.tabla = tabla

    #imprimir: Realiza la impresión y el análisis de contexto de las 
    #           instrucciones del robot, llamando a “analisisContexto” en las 
    #           incorporaciones de alcance
    #tabs: entero, que almacena la cantidad de tabs necesarias para la impresion
    #tablaActual: Referencia a tabla de simbolos
    #primeroTabulado: booleano, es falso cuando la primera impresión no requiere 
    #                 el uso de tabs
    #retorna: una cadena de caracteres que almacena la impresión del árbol 
    #         sintáctico abstracto, para su impresión en caso de que no se 
    #         encuentre ningún error de contexto    
    def imprimir(self, tabs = 0, tablaActual = None, primeroTabulado = True):
        operaciones = {
            "BIN_ARITMETICA",
            "BIN_RELACIONAL", 
            "BIN_BOOLEANA", 
            "UNA_BOOLEANA", 
            "UNA_ARITMETICA"
            }
        expresiones = {
            "EXPR_BOOLEANA",
            "EXPR_ARITMETICA",
            "CARACTER",
            "ID"
        }
        booleanos = {
            'BIN_RELACIONAL',
            'BIN_RELACIONAL',
            'UNA_BOOLEANA'
        
        }
        
        cadena = ""
        if(self.tipo == "PROGRAMA"):
            tablaActual = self.tabla 
            #Si el programa no incluye declaraciones
            if len(self.hijos) == 1:
                cadena +=self.hijos[0].imprimir(tabs,tablaActual) 
            
            #si el programa incluye declaraciones
            else:
                cadena += self.hijos[1].imprimir(tabs,tablaActual) 
                 
             
        else: 
            #Si hay nodo en los que hacer recursión   
            if self.hijos:
              
                if(self.tipo != 'EXPRESION'):
                    
                    if(primeroTabulado):
                    
                        #impresión de la identación
                        
                        cadena += tabs * "  "
                            
                        #actualización de tabs                      
                        if self.tipo == '- var:  ':
                            cadena += self.tipo 
                            tabs += 2
                        else:
                            cadena += self.tipo  + "\n"
                        tabs += 1
                        
                    else:
                        cadena += self.tipo + "\n"
                       
                    #Si el nodo contiene una operación
                    if(self.tipo in operaciones):
                        self.verificarTipos(tablaActual) 
                        cadena += (tabs * "  ") + "- operación:" + str(self.hoja[0]) + "\n"
                        
                        #si es una operación binaria
                        if(len(self.hijos)== 2):
                            cadena += (tabs * "  ") + "- operador izquierdo: "
                            cadena += self.hijos[0].imprimir(tabs+2, tablaActual, False) 
                            cadena += (tabs * "  ") + "- operador derecho: "
                            cadena += self.hijos[1].imprimir(tabs+2,tablaActual, False) 
            
                        else:
                            cadena += (tabs * "  ") + "- operador: " #+ str(self.hijos[0].hijos[0].hoja[0])+"\n"
                            cadena += self.hijos[0].imprimir(tabs+2, tablaActual, False)
                            
                    elif(self.tipo == 'CONDICIONAL'):
                        #Se verifica que la guardia del condicional sea un booleano
                        if self.hijos[0].verificarTipos(tablaActual) != 'bool':
                            raise TypeError("La guardia de un condicional debe ser de tipo booleano.")
                        cadena += (tabs * "  ") + "- guardia: "
                        
                        cadena += self.hijos[0].imprimir(tabs+2,tablaActual, False) 
                        cadena += (tabs * "  ") +"- exito: "
                        cadena += self.hijos[1].imprimir(tabs+1,tablaActual, False) 
            
                        if(len(self.hijos) ==3):
                            cadena += (tabs * "  ") + "- fracaso: "
                            cadena += self.hijos[2].imprimir(tabs+1,tablaActual, False) 
                            
                    elif (self.tipo == 'ITERACION'):
                        if self.hijos[0].verificarTipos(tablaActual) != 'bool':
                            raise TypeError("La guardia de una iteración debe ser de tipo booleano.")
                        cadena += (tabs * "  ") + "- guardia: "
                        cadena += self.hijos[0].imprimir(tabs+1,tablaActual, False)                        
                        cadena += (tabs * "  ") +"- exito: "
                        cadena += self.hijos[1].imprimir(tabs+1,tablaActual, False)
                        
                    elif(self.tipo =="INCORPORACION_ALCANCE"):
                        #pdb.set_trace()
                        tabla = self.tabla
                        tabla.padre = tablaActual
                        #si no tiene declaraciones
                        if len(self.hijos) == 1:
                            cadena += self.hijos[0].imprimir(tabs, tabla) 
                        #si tiene declaraciones
                        else:
                            self.hijos[0].analisisContexto()
                            cadena += self.hijos[1].imprimir(tabs, tabla) 
                            
                    else:    
                        
                        for i in self.hijos:
                            #Se actualiza primero tabulado, para la impresion
                            #de identificadores
                            if(self.tipo == "- var:  " and primeroTabulado):
                                primeroTabulado = False
                                
                                for simbolo in self.hijos:
                                    #Se verifica si la variable fue declarada
                                    
                                    if not tablaActual.buscarSimb(simbolo.hoja[0]):
                                        #pdb.set_trace()
                                        raise ContextError("La variable <%s> no ha sido declarada."%simbolo.hoja[0])  
                                cadena += i.imprimir( 0, tablaActual) 
                            else:
                                cadena += i.imprimir( tabs +1, tablaActual) 
            
                else:
                    #Si es un numero, un booleano, un caracter o un identificador
                    if self.hijos[0].tipo in expresiones:
                       cadena += str(self.hijos[0].hoja[0]) + "\n"
                    
            else:
                cadena += (tabs * "  ") 
                tabs += 1
                #Se imprimen palabras del lenguaje bot
                for i in self.hoja:
                    cadena += str(i) + "\n"
                
        return cadena
    #imprimir: efectua el análisis de contexto a la sección correspondiente a las
    #           declaraciones
    #retorna: Si retorna, el análisis es exitoso, si no, arroja error.             
    def analisisContexto(self):
        global tablaSimb        
        if(self.tipo == "PROGRAMA"):
            #Si el programa no incluye declaraciones
            if len(self.hijos) != 1:
                self.hijos[0].tabla = self.tabla
                self.hijos[0].analisisContexto()
        else:
            #Si hay nodo en los que hacer recursión   
            if self.hijos:
                #Caso en el que tienes declaraciones.
                if (self.tipo == 'LISTA_DECLARACIONES'):
                    for declaraciones in self.hijos:
                        declaraciones.analisisContexto()
                
                elif(self.tipo == 'DECLARACION'):
                    identificadores = ''
                    for hijos in self.hijos[1].hijos:
                        identificadores = identificadores + hijos.hoja[0] + "-"
                    
                    #A este nivel, ya tenemos todos los identificadores    
                    str = self.hijos[0].hoja[0] + self.hoja[0] + identificadores
                    
                    #Se procede a revisar los identificadores.
                    vistoDefault = False
                    condiciones = set()
                    #Si no hay comportamientos, no hay que verificar nada.
                    if (self.hijos[2] == None):
                        return
                    
                    for comportamientos in self.hijos[2].hijos:
                        
                        #Tabla de Símbolos correspondiente a la declaracion actual
                        dic = comportamientos.tabla.simbolos
                        
                        #Tipo de los robot asociados a esta declaración
                        tipo = (self.hijos[0].hoja[0])
                        
                        #Se actualizan todas las variables declaradas dentro
                        #de una declaracion asignandoles el tipo del robot
                        
                        for key in dic:
                            #Estas son las variables que están definidas para un robot
                            #dentro de un comportamiento, el segundo campo es 
                            #el valor que tiene la variable.
                            dic[key] = [tipo,None]
                        
                        #pdb.set_trace()                            
                        #Se verifica para la declaración actual, cada condición.
                        if (comportamientos.hijos[0].hoja):
                            #Aquí se tiene on default, on activation o on deactivation.
                            condicion = comportamientos.hijos[0].hoja[0]
                        else:
                            #aquí se tiene on y una expresión que debe ser booleana.
                            condicion = comportamientos.hijos[0].hijos[0]
                                                        
                            if condicion.verificarTipos(comportamientos.tabla) != 'bool':
                                raise TypeError("La condición de un comportamiento debe ser de tipo booleano.")
                        
                        #Verificamos si hay redeclaraciones de condiciones,
                        #si existe default y si se ha puesto de ultimo.
                        if(condicion in condiciones):
                            raise ContextError("Error en comportamiento <%s>: ya ha sido declarado." %(condicion))
                        else:
                            if(condicion == 'default'):
                                vistoDefault = True
                            elif (vistoDefault):
                                raise ContextError("Error en condición <%s>: no pueden haber condiciones luego de un default." %(condicion))
                        
                        #Se añade la condición actual a un conjunto para verificar
                        #si hay repeticiones en las declaraciones.         
                        condiciones.add(condicion)
                        
                        #Mandamos a verificar las instrucciones de robot.
                        #Tipo de intrucciones del robot que por gramatica
                        #no contienen expresiones
                        sinExpresiones = ['COLECCION','ENTRADA','SALIDA','DIRECCION']
                        for instruccion in comportamientos.hijos[1].hijos:
                            #Si la expresión es de tipo colección, entrada, salida
                            #o no contiene expresiones (tipo up, left, etc)
                            #entonces no se deben verificar tipos.
                            if (instruccion.tipo == "INSTR_ROBOT"):
                                instruccion = instruccion.hijos[0]
                                
                            if (not(instruccion.tipo in sinExpresiones) and not(instruccion.hijos == [])):
                                if(instruccion.tipo == "MOVIMIENTO"):
                                    contenido = instruccion.hijos[1]
                                    tipoVer = "int"
                                else:
                                    contenido = instruccion.hijos[0]
                                    tipoVer = tipo
                                
                                tipoexpr = contenido.verificarTipos(comportamientos.tabla)
                                
                                #Al estar en este punto, ya la expresión
                                #es válida de tipo en sus operadores y operandos.
                                #ahora hay que verificar si es del mismo tipo del robot.
                                if (tipoVer != tipoexpr):
                                    raise TypeError("la instrucción es de tipo <%s> y el robot es de tipo <%s>." %(tipoexpr, tipo))
                    return
    #verificarTipos: encuentra errores de tipo en caso de que existan
    #tablaActual: referencia a la tabla de simbolos asociada al alcance actual 
    #retorna: Si termina, retorna el tipo de la expresión, si no, arroja error 
    #         de tipos.
    def verificarTipos(self, tablaActual):
        dicTipos = {}
        dicExpr = {}
        dicUna = {}
        
        dicTipos["BIN_BOOLEANA"] = "bool"
        dicTipos["BIN_ARITMETICA"] = "int"
        dicTipos["BIN_RELACIONAL"] = "int"

        dicExpr['EXPR_BOOLEANA'] = 'bool'
        dicExpr['EXPR_ARITMETICA'] = 'int'
        dicExpr['CARACTER'] = "char"
        
        dicUna['UNA_ARITMETICA'] = "int"
        dicUna['UNA_BOOLEANA'] = "bool"
        
        if (self.tipo == "EXPRESION"):
            if (self.hijos[0].tipo == "ID"):
                tipo = tablaActual.buscarTipo(self.hijos[0].hoja[0])
                if (tipo == None):
                    raise ContextError("Variable <%s> no ha sido declarada." %(self.hijos[0].hoja[0]))
            else:
                tipo = dicExpr[self.hijos[0].tipo]
            return tipo
        
        elif (self.tipo in dicTipos):
            tipoizq = self.hijos[0].verificarTipos(tablaActual)
            tipoder = self.hijos[1].verificarTipos(tablaActual)
            
            #Verificamos si los operadores son del mismo tipo.
            if (tipoizq != tipoder):
                raise TypeError("Operandos izquerdos de tipo <%s> y operandos derechos de tipo <%s>" %(tipoizq, tipoder))
            
            #Verificamos si el tipo de los operadores coincide con el operador.
            tipoop = dicTipos[self.tipo]
            
            if (tipoizq != tipoop):
                raise TypeError("Tipo <%s> del operador es incompatible al tipo %s de los operandos." %(tipoop, tipoizq))
                
            if (self.tipo == "BIN_RELACIONAL"):
                tipoizq = "bool"
                
            return tipoizq
        elif (self.tipo in dicUna):
            tipooperando = self.hijos[0].verificarTipos(tablaActual)
            tipo = dicUna[self.tipo]
            
            #Se verifica que el tipo de la expresión concuerde con la resta o la negación
            if(tipooperando != tipo):
                raise TypeError("Tipo del operando <%s> es incompatible con el operador <%s>." %(tipooperando, self.hoja[0]))
            
            return tipooperando    


    #imprimir: Realiza la impresión y el análisis de contexto de las 
    #           instrucciones del robot, llamando a “analisisContexto” en las 
    #           incorporaciones de alcance
    #tabs: entero, que almacena la cantidad de tabs necesarias para la impresion
    #tablaActual: Referencia a tabla de simbolos
    #primeroTabulado: booleano, es falso cuando la primera impresión no requiere 
    #                 el uso de tabs
    #retorna: una cadena de caracteres que almacena la impresión del árbol 
    #         sintáctico abstracto, para su impresión en caso de que no se 
    #         encuentre ningún error de contexto    
    def recorrer(self,arboles, tabs = 0, tablaActual = None, primeroTabulado = True):
        operaciones = {
            "BIN_ARITMETICA",
            "BIN_RELACIONAL", 
            "BIN_BOOLEANA", 
            "UNA_BOOLEANA", 
            "UNA_ARITMETICA"
            }
        expresiones = {
            "EXPR_BOOLEANA",
            "EXPR_ARITMETICA",
            "CARACTER",
            "ID"
        }
        booleanos = {
            'BIN_RELACIONAL',
            'BIN_RELACIONAL',
            'UNA_BOOLEANA'
        
        }
        
        cadena = ""
        if(self.tipo == "PROGRAMA"):
            tablaActual = self.tabla 
            #Si el programa no incluye declaraciones
            if len(self.hijos) == 1:
                cadena +=self.hijos[0].recorrer(arboles, tabs,tablaActual) 
            
            #si el programa incluye declaraciones
            else:
                cadena += self.hijos[1].recorrer(arboles, tabs,tablaActual) 
                 
             
        else: 
            #Si hay nodo en los que hacer recursión   
            if self.hijos:
                #pdb.set_trace()
                if(self.tipo != 'EXPRESION'):
                            
                    #actualización de tabs                      
                    if self.tipo == '- var:  ':
                        cadena += self.tipo 
                        tabs += 2
                    else:
                        #pdb.set_trace()
                        lista_declaraciones = None
                        listRobots = execute(self,tablaActual)
                        if listRobots:
                            for lista in listRobots:
                                i = 0
                                #Se busca en todos los árboles de declaraciones.
                                for arbol in arboles:
                                    lista_declaraciones = buscarDeclaracion(arbol,lista[0])
                                    if lista_declaraciones:
                                        execute(lista_declaraciones,tablaActual,lista[1],lista[0])
                                        break
                       
                    #Si el nodo contiene una operación
                    if(self.tipo == 'CONDICIONAL'):
                        #Se verifica que la guardia del condicional sea un booleano
                        if self.hijos[0].verificarTipos(tablaActual) != 'bool':
                            raise TypeError("La guardia de un codicional debe ser de tipo booleano.")

                        evaluarIf = evaluate(self.hijos[0],tablaActual)
                        if(evaluarIf):
                            #Se debe recorrer el éxito.
                            self.hijos[1].recorrer(arboles, tabs+1,tablaActual, False) 
            
                        if((len(self.hijos) ==3) and (not(evaluarIf))):
                            #Se verifica que haya que evaluar el else. Si este existe.
                            self.hijos[2].recorrer(arboles, tabs+1,tablaActual, False) 
                            
                    elif (self.tipo == 'ITERACION'):
                        if self.hijos[0].verificarTipos(tablaActual) != 'bool':
                            raise TypeError("La guardia de una iteración debe ser de tipo booleano.")
                        #pdb.set_trace()    
                        while(evaluate(self.hijos[0],tablaActual)):
                            self.hijos[1].recorrer(arboles, tabs+1,tablaActual, False)
                        
                    elif(self.tipo =="INCORPORACION_ALCANCE"):
                        tablaActual = self.tabla
                        #Se reinicia la tabla cada vez que se comienza.
                        
                        for key in tablaActual.simbolos:
                            tablaActual.simbolos[key] = [tablaActual.simbolos[key][0],None,False,0,0]
                        #si no tiene declaraciones
                        if len(self.hijos) == 1:
                            cadena += self.hijos[0].recorrer(arboles, tabs, tablaActual) 
                        #si tiene declaraciones
                        else:
                            #pdb.set_trace()
                            aux = [self.hijos[0]]
                            for arbol in arboles:
                                aux.append(arbol)
                            arboles = aux
                            cadena += self.hijos[1].recorrer(arboles, tabs, tablaActual) 
                            
                    else:    
                        
                        for i in self.hijos:
                            #Se actualiza primero tabulado, para la impresion
                            #de identificadores
                            if(self.tipo == "- var:  " and primeroTabulado):
                                primeroTabulado = False
                                
                                for simbolo in self.hijos:
                                    #Se verifica si la variable fue declarada
                                    if not tablaActual.buscarSimb(simbolo.hoja[0]):
                                        raise ContextError("La variable <%s> no ha sido declarada."%simbolo.hoja[0])  
                                cadena += i.recorrer(arboles,  0, tablaActual) 
                            else:
                                cadena += i.recorrer(arboles,  tabs +1, tablaActual) 
            
                else:
                    pass
                    
            else:
                pass
                
        return cadena
