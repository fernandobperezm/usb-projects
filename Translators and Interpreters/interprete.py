 #  interprete.py
#  Descripción: Archivo que contiene las funciones evaluate y execute del 
#  intérprete del lenguaje BOT.
#
#  Autores:
#    Br. Leslie Rodrigues, carné: 10-10613.
#    Br. Fernando Pérez, carné: 12-11152.
#
#  Última Modifcación:3/21/2016.
#

from errores import *
import pdb

# Matriz: Representa una matriz posiblemente infinita (de crecimiento dinámico)
# para representar la posición de los robots.
class Matriz:
    #Constructor.
    def __init__(self):
        self.tablero = [[None]]
        self.xMax = 1
        self.yMax = 1
        
    #Ingresar valor al tablero.
    def agregarElemento(self, valor = None, RPosx = 0, RPosy = 0):
        #Se calculan las posiciones nuevas en el tablero.
        if RPosx >= 0:
            TPosx = 2 * RPosx
        else:
            TPosx = (-2 * RPosx) - 1
            
        if RPosy >= 0:
            TPosy = 2 * RPosy
        else:
            TPosy = (-2 * RPosy) - 1
            
        #Se verifica si hay que aumentar el tamaño de la matriz.
        if ( (TPosx >= self.xMax ) or (TPosy >= self.yMax) ):
            #El nuevo máximo estará determinado por el doble de cada posición en el tablero.
            tableroNuevo = [[None for j in range(0,2*TPosy+1)] for i in range(0,2*TPosx+1)]
            #Se copian los elementos del tablero.
            for i in range(0,self.xMax):
                for j in range(0,self.yMax):
                    tableroNuevo[i][j] = self.tablero[i][j]
            
            #Se cambia el tablero.
            self.tablero = tableroNuevo
            self.xMax = 2 * TPosx + 1
            self.yMax = 2 * TPosy + 1
            
        #Se ingresa el valor a la matriz.
        self.tablero[TPosx][TPosy] = valor

    #Recibir un valor del tablero.
    def recibirElemento(self, RPosx = 0, RPosy = 0):
        #Se calculan las posiciones nuevas en el tablero.
        if RPosx >= 0:
            TPosx = 2 * RPosx
        else:
            TPosx = (-2 * RPosx) - 1
            
        if RPosy >= 0:
            TPosy = 2 * RPosy
        else:
            TPosy = (-2 * RPosy) - 1
            
        #Se verifica si la posición es mayor que el tamaño de la matriz.
        if ( (TPosx >= self.xMax ) or (TPosy >= self.yMax) ):
            return None
            
        #Se ingresa el valor a la matriz.
        return self.tablero[TPosx][TPosy]

matriz = Matriz()
 
def execute(nodo,tabla = {} ,comportamiento = None,identificador = None):
    lista = []
    if nodo.tipo == "ACTIVACION":
        
        for id in nodo.hijos[0].hijos:
            #En id.hoja[0] está el robot a activar. 
            #Si el robot no está activado.
            simbolo = id.hoja[0]
            simb = tabla.buscarEstatus(simbolo)
            

            if (not(simb[2])):
                #Se cambia su estatus a activo. 
                simb[2] = True
                tabla.simbolos[simbolo] = simb
            else:
                raise DynamicError("El robot <%s> ya ha sido activado." %(simbolo))
                
            lista.append([simbolo,"activation"])
          
        return lista
            
    elif nodo.tipo == "DESACTIVACION":
        for id in nodo.hijos[0].hijos:
            #En id.hoja[0] está el robot a activar. 
            #Si el robot no está activado.
            simbolo = id.hoja[0]
            simb = tabla.buscarEstatus(simbolo)
            if (simb[2]):
                #Se cambia su estatus a activo.
                simb[2] = False
                tabla.simbolos[simbolo] = simb
            else:
                raise DynamicError("El robot <%s> ya ha sido desactivado." %(simbolo))
            
            lista.append([simbolo,"deactivation"])
            
        return lista   
            
    elif nodo.tipo == "AVANCE":
        for id in nodo.hijos[0].hijos:
            #En id.hoja[0] está el robot a avanzar. 
            #Si el robot está activado, entonces se avanza.
            simbolo = id.hoja[0]
            #Simb[1] es True si el robot está activado, False si no.
            simb = tabla.buscarEstatus(simbolo)
            if (simb[2]):
                pass
            else:
                raise DynamicError("El robot <%s> no puede avanzar porque está desactivado." %(simbolo))
            
            lista.append([simbolo,"advance"])
            
        return lista
    
    elif nodo.tipo == "LISTA_COMPORTAMIENTOS":
        #Dependiendo del comportamiento que se busque (pasado como argumento)
        #se dentendrá a ejecutar cada acción.
        comportamientoCumplido = False
        for comportamientos in nodo.hijos:
            if (comportamientos.hijos[0].hoja):
                condicion = comportamientos.hijos[0].hoja[0]
            else:
                condicion = comportamientos.hijos[0].hijos[0]
                #Actualizamos la tabla de variables para el comportamiento
                actualizarValoresMe(comportamientos,tabla,identificador)
                comportamientoCumplido = evaluate(condicion,comportamientos.tabla)
            
            #Si se pide un default, debe verificarse que ninguna condición se ha cumplido.   
            if ((comportamiento == "advance")):
                if (comportamientoCumplido):
                    executeComportamiento(comportamientos,tabla,identificador)
                    return
                
                if condicion == "default":
                    executeComportamiento(comportamientos, tabla,identificador)
                    return
            
            #Finalmente, si el comportamiento es el que se pide, se manda a ejecutar.
            if (condicion == comportamiento):
                executeComportamiento(comportamientos,tabla,identificador)
                return  
        
        #Si al finalizar la lista de comportamientos definidos para un robot no 
        #se encuentra la condición que debe ejecutarse, debe reportarse un error.        
        #raise DynamicError("El robot <%s> no tiene definida la condición <%s>." %(identificador,comportamiento))
            
def executeComportamiento(nodo, tablaGlobal,identificador):
    global matriz
    #Se obtiene la tabla necesaria para este comportamiento.
    tabla = tablaGlobal.buscarTabla(identificador)
    #Se actualiza el valor de me con el valor del robot antes de comenzar.
    valoresMe = nodo.tabla.simbolos["me"]
    simbolos = tabla.simbolos[identificador]
    valoresMe[1] = simbolos[1]
    nodo.tabla.simbolos["me"] = valoresMe
    #Se itera sobre toda la lista de comportamientos.
    for instrucciones in nodo.hijos[1].hijos:
        #Debemos verificar si hay más de una instrucción de robot en el comportamiento.
        if instrucciones.tipo == "INSTR_ROBOT":
            #Aquí hay más de una.
            comportamiento = instrucciones.hijos[0]
        else:
            #Aquí hay solo una.
            comportamiento = instrucciones
        if comportamiento.tipo == "ALMACENAMIENTO":
            #Se obtiene la evaluación de la expresión. Luego, se actualiza el 
            #valor de me en la tabla del comportamiento y el valor en la
            #tabla de variables para que en otros comportamientos se obtenga este valor.
            
            simbolos[1] = evaluate(comportamiento.hijos[0],nodo.tabla)
            valoresMe[1] = simbolos[1]
            nodo.tabla.simbolos["me"] = valoresMe
            tabla.simbolos[identificador] = simbolos
            
        elif comportamiento.tipo == "COLECCION":
            #Obtener posición del robot en la matriz.
            PosX = tabla.simbolos[identificador][3]
            PosY = tabla.simbolos[identificador][4]
            #Se obtiene el valor de la matriz.
            entrada = matriz.recibirElemento(PosX,PosY)
            
            if (entrada == None):
                raise DynamicError("El valor en la matriz no está inicializado.")
            valor =comportamientoInadecuado("Coleccion",simbolos[0],entrada)
            
            #Se verifica si se aplica a una nueva variable o a me.
            if (comportamiento.hijos == []):
                #Aquí debe aplicarse a me.
                simbolos[1] = valor
                valoresMe[1] = simbolos[1]
                nodo.tabla.simbolos["me"] = valoresMe
                
                tabla.simbolos[identificador] = simbolos
            else:
                #Aquí debe apliarse a otra variable definida en expresión.
                variable = comportamiento.hijos[0].hoja[0]
                valores = nodo.tabla.simbolos[variable]
                valores[1] = valor
                nodo.tabla.simbolos[variable] = valores
            
        elif comportamiento.tipo == "SOLTADO":
            #Se evalúa la expresión.
            expr = comportamiento.hijos[0]
            evaluado = evaluate(expr,nodo.tabla)
            
            #Se obtiene la posición del robot.
            PosX = simbolos[3]
            PosY = simbolos[4]
            
            #Se obtiene 
            matriz.agregarElemento(evaluado,PosX,PosY)
            
        elif comportamiento.tipo == "MOVIMIENTO":
            if comportamiento.hijos == []:
                #Aquí, solamente está la dirección.
                direccion = comportamiento.hoja[0].hoja[0]
                expr = 1
            else:
                #Aquí, está la dirección y la expresión.
                direccion = comportamiento.hijos[0].hoja[0]
                expr = evaluate(comportamiento.hijos[1],nodo.tabla)
                #VERIFICAR QUE SEA NO NEGATIVA.
                
            #Dependiendo de la expresión y la instrucción, se calcula la nueva posición    
            if direccion == "left":
                posx = simbolos[3]
                simbolos[3] = posx - expr
            elif direccion == "right":
                posx = simbolos[3]
                simbolos[3] = posx + expr
            elif direccion == "down":
                posy =  simbolos[4]
                simbolos[4] = posy - expr
            elif direccion == "up":
                posy = simbolos[4]
                simbolos[4] = posy + expr
            
            tabla.simbolos[identificador] = simbolos
            
        elif comportamiento.tipo == "ENTRADA":
            #Se pide el valor al usuario.
            entrada = input("\nBase humana, ingrese el valor que desee almacenar.\n")
            valor = comportamientoInadecuado('Lectura',simbolos[0],entrada)
                
            
            #Se verifica si se aplica a una nueva variable o a me.
            if (comportamiento.hijos == []):
                #Aquí debe aplicarse a me.
                simbolos[1] = valor
                valoresMe[1] = simbolos[1]
                nodo.tabla.simbolos["me"] = valoresMe
                
                tabla.simbolos[identificador] = simbolos
            else:
                #Aquí debe apliarse a otra variable definida en expresión.
                variable = comportamiento.hijos[0].hoja[0]
                valores = nodo.tabla.simbolos[variable]
                valores[1] = valor
                nodo.tabla.simbolos[variable] = valores
                
        elif comportamiento.tipo == "SALIDA":
            
            print("%s"%(simbolos[1]), end = '')
            
# evaluate: evalua una expresion 
# nodo: nodo del arbol sintactico abstracto
# evaluacion: resultado de evaluar la expresion
def evaluate(nodo,tabla):
    evaluacion = None
    if nodo.tipo == 'EXPRESION':
        nodo = nodo.hijos[0]
        
        if nodo.tipo == 'EXPR_BOOLEANA':
            if nodo.hoja[0] == "true":
                evaluacion = True
            else:
                evaluacion = False
        elif nodo.tipo == 'EXPR_ARITMETICA':
            evaluacion = int(nodo.hoja[0])
        
        elif nodo.tipo == 'CARACTER':
            evaluacion = nodo.hoja[0][1]
           
        else:
            if (len(tabla.simbolos[nodo.hoja[0]]) > 2):
                if (tabla.simbolos[nodo.hoja[0]][2] == None) :
                    #Se tiene un robot.
                    raise DynamicError("Uso inadecuado: No puede usarse en una expresion un robot que no ha sido activado.")
                    
            if (tabla.simbolos[nodo.hoja[0]][1] == None):
                #Se tiene una variable de comportamiento.
                raise DynamicError("Uso inadecuado: No puede usarse en una expresion un robot que no haya sido inicializado.")    
                
            evaluacion = tabla.simbolos[nodo.hoja[0]][1]
            
    elif nodo.tipo == 'UNA_ARITMETICA':
        nodo = nodo.hijos[0]
        evaluacion = -evaluate(nodo,tabla)
    elif nodo.tipo == 'UNA_BOOLEANA':
        nodo = nodo.hijos[0]
        evaluacion = not evaluate(nodo,tabla)
    elif nodo.tipo == 'BIN_BOOLEANA':
        if nodo.hoja[0] == "\\/":
            evaluacion = evaluate(nodo.hijos[0],tabla) or evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == "/\\":
            evaluacion = evaluate(nodo.hijos[0],tabla) and evaluate(nodo.hijos[1],tabla)
    elif nodo.tipo == 'BIN_RELACIONAL':
        
        if nodo.hoja[0] == '<=':
            evaluacion = evaluate(nodo.hijos[0],tabla) <= evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '<':
            evaluacion = evaluate(nodo.hijos[0],tabla) < evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '>=':
            evaluacion = evaluate(nodo.hijos[0],tabla) >= evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '>':
            evaluacion = evaluate(nodo.hijos[0],tabla) > evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '=':
            evaluacion = evaluate(nodo.hijos[0],tabla) == evaluate(nodo.hijos[1],tabla)
    elif nodo.tipo == 'BIN_ARITMETICA':
        
        if nodo.hoja[0] == '+':
            evaluacion = evaluate(nodo.hijos[0],tabla) + evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '-':
            evaluacion = evaluate(nodo.hijos[0],tabla) - evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '*':
            evaluacion = evaluate(nodo.hijos[0],tabla) * evaluate(nodo.hijos[1],tabla)
        elif nodo.hoja[0] == '/':
            divisor = evaluate(nodo.hijos[1],tabla)
            if divisor == 0:
                raise DynamicError("División entre cero.")
            evaluacion = evaluate(nodo.hijos[0],tabla) // divisor
        elif nodo.hoja[0] == '%':
            evaluacion = evaluate(nodo.hijos[0],tabla) % evaluate(nodo.hijos[1],tabla)
            
    #print(evaluacion)
    return evaluacion
    
    
#imprimir: efectua el análisis de contexto a la sección correspondiente a las
#           declaraciones
#retorna: Si retorna, el análisis es exitoso, si no, arroja error.             
def buscarDeclaracion(ast,identificador):        
    if(ast.tipo == "PROGRAMA"):
        #Si el programa no incluye declaraciones
        if len(ast.hijos) != 1:
            return buscarDeclaracion(ast.hijos[0],identificador)
    else:
        #Si hay nodo en los que hacer recursión   
        if ast.hijos:
            #Caso en el que tienes declaraciones.
            if (ast.tipo == 'LISTA_DECLARACIONES'):
                for declaraciones in ast.hijos:
                     nodo = buscarDeclaracion(declaraciones,identificador)
                     if nodo:
                        return nodo
                return None
            
            elif(ast.tipo == 'DECLARACION'):
            
                for hijos in ast.hijos[1].hijos:
                    if (hijos.hoja[0] == identificador):
                        return ast.hijos[2]
                
                return None
              
def actualizarValoresMe(nodo, tablaGlobal, identificador):
    #Se obtiene la tabla necesaria para este comportamiento.
    tabla = tablaGlobal.buscarTabla(identificador)
    #Se actualiza el valor de me con el valor del robot antes de comenzar.
    valoresMe = nodo.tabla.simbolos["me"]
    simbolos = tabla.simbolos[identificador]
    valoresMe[1] = simbolos[1]
    nodo.tabla.simbolos["me"] = valoresMe
    
def comportamientoInadecuado(comportamiento,simbolo,entrada):
    if (simbolo == "int"):
        try:
            valor = int(entrada)
        except ValueError:
            raise DynamicError(comportamiento + " inadecuada: El valor introducido no es del mismo tipo del robot (int).")
                
                
    elif (simbolo == "char"):
                
        if (len(entrada) == 1 ):
            valor = entrada[0]
        elif(len(entrada)==2 and entrada[0]=='\\' and (entrada[1]=='s'or entrada[1]=='t' or entrada[1] == 'n')):
         
            valor = entrada
        else:
            raise DynamicError(comportamiento +" inadecuada: El valor introducido no es del mismo tipo del robot (char).")
                        
    elif (simbolo == "bool"):
        if(entrada == 'true' or entrada == 'false'):
        
            valor = entrada
        else:
            raise DynamicError(comportamiento +" inadecuada: El valor introducido no es del mismo tipo del robot (bool).")
    return valor