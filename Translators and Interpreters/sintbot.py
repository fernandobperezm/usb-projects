# sintbot.py
#
# Descripción: Este es el módulo principal del parser.
#
# Estudiantes:
#   Br. Leslie Rodrigues, carné: 10-10613.
#   Br. Fernando Pérez, carné: 12-11152.
#
# Última modificación: 03/07/2016.
#

import ply.yacc as yacc
from errores import *
from estructuras_de_datos import *
import pdb

precedence = [
    
    ('left','TkConjuncion','TkDisyuncion'),
    ('left','TkMenor','TkMenorIgual','TkMayor','TkMayorIgual','TkIgual'),
    ('left','TkSuma','TkResta'),
    ('left','TkMult','TkDiv','TkMod'),
    ('right','TkNegacion'),
    ('right','TkParAbre')
    ]

global tablaSimb
tablaSimb = TablaSimb()
                     
def p_inicio(t):
    '''inicio : programa'''
    t[0] = t[1] 

def p_programa(t):
    '''programa : TkCreate lista_declaraciones TkExecute secuencia_control TkEnd 
                | TkExecute secuencia_control TkEnd'''
    global tablaSimb
    #pdb.set_trace()
    if(len(t)>4):         
        for declaracion in t[2].hijos:
            tipo = (declaracion.hijos[0].hoja[0])
            
            for hijo in declaracion.hijos[1].hijos:
                simbolo = ((hijo.hoja[0]))
                if(not(tablaSimb.anadirSimb(simbolo,tipo,None,False,0,0))):
                    raise ContextError("Redeclaración de variable en: <%s>." %(simbolo))
        
        t[0] = nodo("PROGRAMA",[t[2],t[4]], [t[1],t[3],t[5]],tablaSimb)
        
    else:
        t[0] = nodo("PROGRAMA",[t[2]], [t[1],t[3]],tablaSimb)

    
def p_lista_de_declaraciones(t):
    '''lista_declaraciones : declaracion
                           | declaracion lista_declaraciones '''
    if(len(t)>2):       
        
        lista = [t[1]] 
        lista +=  t[2].hijos
        t[0] = nodo("LISTA_DECLARACIONES",lista)
    else:    
        t[0] = nodo("LISTA_DECLARACIONES",[t[1]])
    
    
    
        
def p_declaraciones(t):
    ''' declaracion : tipo TkBot IDs lista_comportamientos TkEnd 
                    | tipo TkBot IDs vacio TkEnd'''
    t[0] = nodo("DECLARACION",[t[1],t[3],t[4]],[t[2],t[5]])
    
def p_tipo(t):
    '''tipo : TkInt
            | TkChar
            | TkBool'''
    t[0] = nodo('TIPO',[],[t[1]])
    
def p_lista_de_comportamiento(t):
    '''lista_comportamientos : comportamiento
                             | comportamiento lista_comportamientos  '''
    if(len(t)>2):       
        lista = [t[1]] 
        lista += t[2].hijos
        t[0] = nodo("LISTA_COMPORTAMIENTOS",lista)
    else:    
        t[0] = nodo("LISTA_COMPORTAMIENTOS",[t[1]])
            
def p_comportamiento(t):
    ''' comportamiento : TkOn condicion TkDosPuntos secuencia_robot TkEnd'''
    
    tabla = TablaSimb({})
    #t[4].hijos es la lista de secuencias de robot.
    if (t[4].tipo == "SECUENCIACION"):
        for instruccion in t[4].hijos:
            tipo = instruccion.hijos[0].tipo
            
            if (tipo in ["COLECCION","ENTRADA"]):
                #Se verifica si la colección o la entrada declaran variables.
                if(instruccion.hijos[0].hijos != []):
                    simbolo = instruccion.hijos[0].hijos[0].hoja[0]
                    #Dado que para este momento, no se tiene el tipo del robot, 
                    #se usa un tipo "generico" que nos permite agregar la variable 
                    #a la tabla.
                    if(not(tabla.anadirSimb(simbolo,"generico"))):
                        raise ContextError("Redeclaración de variable en: <%s>"%simbolo)
    else:
        tipo = t[4].hijos[0].tipo
        if (tipo in ["COLECCION","ENTRADA"]):
            #Se verifica si la colección o la entrada declaran variables.
            if(t[4].hijos[0].hijos != []):
                simbolo = t[4].hijos[0].hijos[0].hoja[0]
                #Dado que para este momento, no se tiene el tipo del robot, 
                #se usa un tipo "generico" que nos permite agregar la variable 
                #a la tabla.
                if(not(tabla.anadirSimb(simbolo,"generico"))):
                    raise ContextError("Redeclaración de variable en: <%s>"%simbolo)
    
    tabla.anadirSimb("me","generico")
        
    t[0] = nodo("LISTA_COMPORTAMIENTO",[t[2],t[4]],[t[1],t[3],t[5]],tabla)
    
#INSTRUCCIONES DEL CONTROLADOR #################################################

def p_instruccion_control_secuenciacion(t):
    '''secuencia_control : controlador
                         | controlador secuencia_control'''
    
    if(len(t)>2):
        #Secuencia de las primeras dos intrucciones
        if(t[2].tipo != "SECUENCIACION"):
            t[0] = nodo("SECUENCIACION",[t[1],t[2]])
        else:#secuencias apartir de la tercera instruccion
            lista = [t[1]]
            lista +=  t[2].hijos
            t[0] = nodo("SECUENCIACION",lista)
            
    else:
        #Si hay una sola instruccion no es una secuencia    
        t[0] = t[1]
        
def p_instruccion_control(t):
    '''controlador : activacion
                   | avance
                   | desactivacion
                   | condicional
                   | iteracion
                   | inc_alcance 
                   '''
    t[0] = t[1]
      
def p_instruccion_control_activacion(t):
    '''activacion : TkActivate IDs TkPunto''' 
    t[0] = nodo('ACTIVACION',[t[2]], [t[1],t[3]])
    
    
def p_intruccion_control_avance(t):
    'avance : TkAdvance IDs TkPunto'
    t[0] = nodo('AVANCE',[t[2]], [t[1],t[3]])    
    
    
def p_intruccion_control_desactivacion(t):
    'desactivacion : TkDeactivate IDs TkPunto'
    
    t[0] = nodo('DESACTIVACION',[t[2]], [t[1],t[3]])
        
    
def p_instruccion_control_condicional(t):
    '''condicional : TkIf expresion TkDosPuntos secuencia_control TkEnd
                   | TkIf expresion TkDosPuntos secuencia_control TkElse TkDosPuntos secuencia_control TkEnd'''
    
    if(len(t)>6):
        t[0] = nodo("CONDICIONAL",[t[2],t[4],t[7]], [t[1],t[3],t[5],t[6],t[8]])
    else:
         t[0] = nodo("CONDICIONAL",[t[2],t[4]], [t[1],t[3],t[5]])

def p_instruccion_control_iteracion(t):
    ''' iteracion : TkWhile expresion TkDosPuntos secuencia_control TkEnd '''
    t[0] = nodo("ITERACION",[t[2],t[4]], [t[1],t[3],t[5]])

def p_instruccion_control_incorporacion_alcance(t):
    '''inc_alcance : TkCreate lista_declaraciones TkExecute secuencia_control TkEnd 
                   | TkExecute secuencia_control TkEnd'''
    tabla = TablaSimb({},None)
    if(len(t) > 4):            
        for declaracion in t[2].hijos:
            tipo = (declaracion.hijos[0].hoja[0])
            
            for hijo in declaracion.hijos[1].hijos:
                simbolo = ((hijo.hoja[0]))
                
                if(not(tabla.anadirSimb(simbolo,tipo,None,False,0,0))):
                    raise ContextError("Redeclaración de variable en: <%s>" %simbolo)
                    
        t[0] = nodo("INCORPORACION_ALCANCE",[t[2],t[4]], [t[1],t[3],t[5]],tabla)
    else:
        t[0] = nodo("INCORPORACION_ALCANCE",[t[2]], [t[1],t[3]],tabla)
    
#INTRUCCIONES DEL ROBOT ########################################################

def p_instruccion_robot_secuenciacion(t):
    '''secuencia_robot : robot
                       | robot secuencia_robot '''
    if(len(t)>2):
        #Secuencia de las primeras dos intrucciones
        if(t[2].tipo != "SECUENCIACION"):
            t[0] = nodo("SECUENCIACION",[t[1],t[2]])
        else:#secuencias apartir de la tercera instruccion
            lista = [t[1]]
            lista += t[2].hijos
            t[0] = nodo("SECUENCIACION",lista)
            
    else:
        #Si hay una sola instruccion no es una secuencia    
        #t[0] = nodo("CONTROLADOR",[t[1]])
        t[0] = t[1]
        
def p_instruccion_robot(t):
    '''robot : almacenamiento
             | coleccion
             | soltado
             | movimiento
             | entrada
             | salida
             '''
    t[0] = nodo("INSTR_ROBOT",[t[1]])

def p_instruccion_robot_almacenamiento(t):
    ''' almacenamiento : TkStore expresion TkPunto'''
    t[0] = nodo("ALMACENAMIENTO",[t[2]], [t[1],t[3]])
    
def p_instruccion_robot_coleccion(t):
    '''coleccion : TkCollect TkPunto
                 | TkCollect TkAs identificador TkPunto'''
    
    if(len(t)>3):
        
        t[0] = nodo("COLECCION",[t[3]], [t[1],t[2],t[4]])
    else:
        t[0] = nodo("COLECCION",[], [t[1],t[2]])
        
def p_instruccion_robot_soltado(t):
    ''' soltado : TkDrop expresion TkPunto'''
    
    t[0] = nodo("SOLTADO",[t[2]], [t[1],t[3]]) 

def p_instruccion_robot_movimiento(t):
    ''' movimiento : direccion TkPunto
                   | direccion expresion TkPunto'''
    if(len(t)>3):
       t[0] = nodo("MOVIMIENTO",[t[1],t[2]], [t[3]])
    else:
       t[0] = nodo("MOVIMIENTO",[], [t[1],t[2]])

def p_instruccion_robot_entrada(t):
    ''' entrada : TkRead TkPunto
                | TkRead TkAs identificador TkPunto'''
    if(len(t)>3):
        t[0] = nodo("ENTRADA",[t[3]], [t[1],t[2],t[4]])
    else:
        t[0] = nodo("ENTRADA",[], [t[1],t[2]])

def p_instruccion_robot_salida(t):
    ' salida : TkSend TkPunto'
    t[0] = nodo("SALIDA",[], [t[1],t[2]])
#condicion
def p_instruccion_robot_condicion(t):
    '''condicion : TkActivation
                 | TkDeactivation
                 | TkDefault
                 | expresion'''
    if((t[1] == 'activation') or(t[1] == 'default') or (t[1] == 'deactivation')) :         
        t[0] = nodo("CONDICION",[],[t[1]])
    else:    
        t[0] = nodo("CONDICION",[t[1]])
    
#direccion
def p_direccion(t):
    '''direccion : TkUp
                 | TkDown
                 | TkLeft
                 | TkRight'''
                 
    t[0] = nodo("DIRECCION",[],[t[1]])
           
##identificadores ##############################################################
def p_lista_de_identificadores(t):
    '''IDs : identificador TkComa IDs
           | identificador '''

    if t[1].hoja[0] == "me":
        raise ContextError("Error de contexto: Ningún robot puede declararse como me.")
        
    if(len(t)>2):       
        
        lista = [t[1]]
        lista +=  t[3].hijos
        t[0] = nodo("- var:  ",lista,t[2])
    else:    
        t[0] = nodo("- var:  ",[t[1]])

    
    
def p_identificador(t):
    'identificador : TkIdent'
    t[0] = nodo("ID",[],[t[1]])
# EXPRESIONES ##################################################################
def p_expresion(t):
    '''expresion : booleano
                 | numero 
                 | caracter
                 | identificador'''
                 
    t[0] = nodo('EXPRESION',[t[1]])

def p_expresion_caracter(t):
    'caracter : TkCaracter'
    t[0] = nodo('CARACTER',[],[t[1]])
    
def p_expresion_numero(t):
    'numero : TkNum'
    
    t[0] = nodo('EXPR_ARITMETICA',[],[t[1]])
    
def p_expresion_booleano(t):
    '''booleano : TkTrue
                | TkFalse'''
    t[0] = nodo('EXPR_BOOLEANA',[], [t[1]])
    
# Agrupacion
def p_expresion_agrupación(t):
    'expresion : TkParAbre expresion TkParCierra'
    t[0] = t[2]
     
# Operaciones Unarias
def p_expresion_opunaria_negativo(t):
    'expresion : TkResta expresion'
    t[0] = nodo('UNA_ARITMETICA',[t[2]],[t[1]])

def p_expresion_opunaria_negacion(t):
    'expresion : TkNegacion expresion '  
    t[0] = nodo('UNA_BOOLEANA',[t[2]], [t[1]])
    
# Operaciones binarias

def p_expresion_opbinaria_booleana(t):
    '''expresion : expresion TkConjuncion expresion
                 | expresion TkDisyuncion expresion''' 
                
    t[0] = nodo('BIN_BOOLEANA',[t[1],t[3]], [t[2]])

def p_expresion_opbinaria_relacional(t):
    '''expresion : expresion TkMenorIgual expresion
                 | expresion TkMayor expresion
                 | expresion TkMayorIgual expresion
                 | expresion TkMenor expresion
                 | expresion TkIgual expresion  
                 '''
                
    t[0] = nodo('BIN_RELACIONAL',[t[1],t[3]], [t[2]])  

        
def p_expresion_opbinaria_aritmetica(t):
    '''expresion : expresion TkSuma expresion
                 | expresion TkResta expresion
                 | expresion TkMult expresion
                 | expresion TkDiv expresion
                 | expresion TkMod expresion'''
                 
    t[0] = nodo('BIN_ARITMETICA',[t[1],t[3]], [t[2]])


# produccion vacia
def p_vacio(t):
    'vacio : '
    pass

# manejo de errores    
def p_error(t):
    raise SyntaxError("Error de sintaxis en: <%s>" %(t.value))

