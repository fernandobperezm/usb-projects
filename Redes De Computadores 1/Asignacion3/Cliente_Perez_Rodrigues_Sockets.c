/*
    Universidad Simón Bolívar.
    
    Descripción: Este archivo contiene el código fuente que maneja el cliente
    para la asignación 3.

    Autores:
        Br. Fernando Pérez, carné: 12-11152.
        Br. Leslie Rodrigues, carné: 10-10613.
 
    Última modificación: 27/06/2016.

*/

#include <stdio.h>       // Para las funciones de entrada y salida estándar.
#include <sys/socket.h>  // Para los sockets.
#include <arpa/inet.h>   // Para los sockets.
#include <stdlib.h>      // Para la librería estándar de C.
#include <string.h>      // Para el manejo de strings.
#include <unistd.h>      // Para el uso de write y close.
#include <sys/types.h>   // Para el manejo de tipos de C.
#include <errno.h>       // Para errno
#include <time.h>        // Para las funciones de tiempo.

#define TAM 255 // Tamaño máximo del buffer


int main(int argc,char *argv[]){
    
    
    
    // Declaración de variables ------------------------------------------------
    
    int s;                           // Descriptor del Sockect
    struct sockaddr_in servAddr;     // Estructura para el manejo de la dirección del servidor.
    struct sockaddr_in clientAddr;   // Estructura para el manejo de la dirección de los clientes.
    char buffer[TAM];                // Buffer 
    unsigned int longitudRespCiente; // Longitud de mensaje de respuesta de cliente.
    int longRespServidor;            // Longitud de mensaje de respuesta del servidor.
    int largo;                       // Para la longitud de los argumentos.
    unsigned short serverPort;       // Puerto del servidor
    char *nombreModuloAtencion;      // nombre  de  dominio  o  la  dirección  IP V4
    char *op;                        // Caracter que indica si es entrada o salida
    int identificacionVehiculo;      // Codigo que identifica un vehiculo
    char *p;                         // Apuntador para verificación de errores.
    long conv;                       // Entero para verificar la conversión de enteros por arugmenteos.
    int i;                           // Contador de propósito general.
    char stringMensaje[TAM];         // Estructura de mensaje para el envío al servidor.
    char *fechaHora;                 // Cadena de caracteres para el manejo de la fecha y la hora.
    char *stringId;                  // Cadena de caracteres para el manejo del ID del vehículo.


    // Verificación y almacenado de argumentos ---------------------------------
    
    //Verificación del número de argumentos
    if (argc != 9){
        fprintf(stderr,"Uso: sem_cli -d <nombre_módulo_atención> -p <puerto_sem_svr> -c <op> -i\
 <identificación_vehiculo>\n");
        return 2;
    }
    
    // Verificación de Flags repetidos
    if (argv[1] == argv[3] || argv[3] == argv[5] || argv[5] == argv[7] || argv[7] == argv[1]){
        printf("Error: Existen Flags repetidos.\n");
        return 2;
    }
    
    //Para cada flag. i es la posicion donde se espera que el flag se encuentre
    //                i+1  es la posicion do se espera que se encuentre el argumento asociado al flag i
    for (i = 1; i<argc -1; i += 2 ){
        largo = strlen(argv[i]);
        // Se verifica que el flag tenga el formato valido: -<caracter>
        if(argv[i][0] != '-' || largo != 2){
            fprintf(stderr,"Error: %s no es un Flag válido.\n",argv[i]);
    		return 2;
        }
        
        
        switch(argv[i][1]){
	        case 'd': // Dominio
		        
		        nombreModuloAtencion = strdup(argv[i+1]);
		        
		        break;
	        case 'p': // Puerto del servidor
	        
	            /* Manejo correcto de la conversión. */
                errno = 0; //Variable de errno.h para el manejo de errores.
                conv = strtol(argv[i+1], &p, 10); // Se pasa el string, donde guardará el \0 y la base a convertir.
                
                // Chequeo de errores:
                //      El primero verifica que sea un entero, que haya habido una conversión.
                //      El segundo verifica se haya terminado correctamente con \0.
                //      El tercero verifica que el número no sea un puerto mayor a los posibles.
                if (errno != 0 || *p != '\0' || conv > 65536 || conv < 1024) {
                    //Aquí hay errores.
                    fprintf(stderr,"El número de puerto es inválido. El rango válido es de 1024 hasta 65536.\n");
                    return 3;
                } else {
                    // Aquí no hay errores.
                    serverPort = (unsigned short) conv; 
                }
		        break;
	        case 'c': // opcion: entrada o salida
	        
	            if((strcmp(argv[i+1],"e") != 0)&& (strcmp(argv[i+1],"s") != 0)){
	                printf("Error: argumento invalido. ");
	                printf("Usted introdujo %s.\n",argv[i+1]);
	                printf("Utilice\n");
	                printf("    e: para entrada.\n    s: para salida.\n");
	                return 3;
	            }
	            // Se almacena la opcion
		        op = strdup(argv[i+1]);
	    	 
	    	    break;
	        case 'i': //identificador del vehiculo
	        
		        /* Manejo correcto de la conversión. */
                errno = 0; //Variable de errno.h para el manejo de errores.
                conv = strtol(argv[i+1], &p, 10); // Se pasa el string, donde guardará el \0 y la base a convertir.
                stringId = strdup(argv[i+1]);
                // Chequeo de errores:
                //      El primero verifica que sea un entero, que haya habido una conversión.
                //      El segundo verifica se haya terminado correctamente con \0.
                //      El tercero verifica que el número no sea un puerto mayor a los posibles.
                if (errno != 0 || *p != '\0' || conv < 0 ) {
                    //Aquí hay errores.
                    fprintf(stderr,"Error: Argumento inválido.\n El identificador del vehículo debe ser un entero positivo.\n");
                    return 3;
                } else {
                    // Aquí no hay errores.
                    identificacionVehiculo = (unsigned short) conv; 
                }
	    	    break;
	        default: // Si el flag no corresponde a un case es inválido
		        printf("Error: Flag invalido.\n");
		        printf("%s no es un Flag válido.\n",argv[i]);
		        return 1;
        }
    }
    
    
    // Creación del mensaje.
    memset(stringMensaje, 0, sizeof(stringMensaje));
    if (!strcmp(op,"e")) {
        // Si la operación es de entrada, se manda el byte de entrada y el byte de puesto.
        strcpy(stringMensaje,"02");
    } else{
        // En caso contratio la operacion es de salida, se manda el byte de salida
        strcpy(stringMensaje,"1");
    }
    strcat(stringMensaje, stringId);
    
    // Creación del socket -----------------------------------------------------
    // El socket del lado del cliente tiene la característica de que esperará
    // solamente 2 segundos para la respuesta con el servidor. Si no recibe tal
    // respuesta, intentará dos veces más. Si al último intento no se tiene
    // respuesta alguna, el programa reportará un error.
    if  ((s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0){
        printf("Error: Creación del socket fallida.");
        return 1;
    }

    struct timeval tv; // Se crea la estructura para especificar el tiempo de espera.
    tv.tv_sec = 2;     // Se indica la cantidad de segundos.
    tv.tv_usec = 0;    // Se indica la cantidad de microsegundos.
    
    // La función setsockopt toma un socket creado, un nivel, la opción y las 
    // configuraciones de la opción. En nuestro caso, le indica al socket
    // que su tiempo de espera por respuesta es de 2 segundos.
    setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval));
    
    //Constructor de la dirección de la estructura del servidor
    memset(&servAddr, 0,  sizeof(servAddr)); //Se llena de ceros la estructura.
    servAddr.sin_family=PF_INET; 
    servAddr.sin_port=htons(serverPort); // Puerto del Servidor
    servAddr.sin_addr.s_addr = inet_addr(nombreModuloAtencion); //Dirección IP del servidor
    
    // Envío y recepción del mensaje, en intervalos de 2seg si no se consigue 
    // respuesta. Máximo tres intentos.
    for (i=0;i<3;i++){
        // Envío del mensaje.
        if (sendto(s, stringMensaje, strlen(stringMensaje),0, (struct sockaddr *)&servAddr, sizeof(servAddr))<0){
            
            fprintf(stderr,"Lo lamentamos, el envío del mensaje al servidor ha fallado.\n");
            return 1;
        }
        
        // Recepción.
        if ((longRespServidor = recvfrom(s, buffer, TAM, 0, (struct sockaddr *)&clientAddr, &longitudRespCiente))<0){
            if (i == 2){
                fprintf(stderr,"Lo lamentamos, no se ha recibido respuesta del servidor y no podemos procesar su solicitud.\n");
                return 1;
            }
        } else{
            break;
        }   
    }
    
    //Manejo del mensaje por parte del cliente.
    buffer[longRespServidor] = '\0'; // Se indica el fin del mensaje.
    switch (buffer[0]) {
        case '0':
            // Se verifica si se puede pasar o no al estacionamiento.
            switch (buffer[1]) {
                //En 0 se puede pasar.
                case '0':
                    fechaHora = strndup(&(buffer[2]), 17); // Se obtiene la fecha y la hora de entrada mandada por el servidor.
                    char *idVehiculo = strdup(&(buffer[20])); // Se obtiene el id del vehículo.
                    
                    //Se imprime el ticket de entrada.
                    printf("\t\t\tBienvenidos al Estacionamiento Moriah (EM).\n");
                    printf("\t\t\t\tEste es su ticket de entrada.\n");
                    printf("\tTarifa:\n"); 
                    printf("\t\tPrimera hora: Bs. 80.\n");
                    printf("\t\tFracción de 1 hora: Bs. 30\n");
                    printf("\tEstos son sus datos:\n");
                    printf("\t\tVehículo: %s\n",idVehiculo);
                    printf("\t\tFecha y hora de entrada: %s\n",fechaHora);
                    printf("\t\t\tGracias por venir al Estacionamiento Moriah (EM).\n");
                    
                    //Para que no hayan fugas de memoria.
                    free(fechaHora);
                    free(idVehiculo);
                    break;
                // En 1 no hay puestos disponibles.
                case '1':
                    fprintf(stderr,"Disculpe, ya no hay puestos disponibles.\n");
                    printf("\t\t\tGracias por venir al Estacionamiento Moriah (EM).\n");
                    return 1;
                //En 2 es que el carro ya está estacionado.
                case '2':
                    fprintf(stderr,"Disculpe, no puede ingresar un vehículo con un identificador ya estacionado.\n");
                    printf("\t\t\tGracias por venir al Estacionamiento Moriah (EM).\n");
                    return 1;
            }
            break;
        case '1':
            switch(buffer[1]){ //indica si el vehiculo esta en el estacionamiento
                case '0': //el vehiculo estaba en el estacionamiento
                
                    //Se imprime el ticket de salida
                    printf("\t\t\tGracias por venir al Estacionamiento Moriah (EM).\n");
                    printf("\t\t\t\tEste es su ticket de salida.\n");
                    printf("\t\tVehículo: %d\n",identificacionVehiculo);
                    printf("\t\tMonto cancelado: %s",&(buffer[19]));
                    buffer[19] = '\0';
                    printf("\t\tFecha y hora de salida: %s\n",&(buffer[2]));
                    break;
                case '2': //el vehiculo no estaba en el estacionamiento
                    // Se muestra mensaje 
                    printf("Disculpe, el identificador que introdujo no corresponde a ");
                    printf("un vehículo en el estacionamiento.\n");
                    printf("\t\t\tGracias por venir al Estacionamiento Moriah (EM).\n");
                    break;
            }
            break;
    }
    // Cierra el socket
    if (close(s)<0) { 
        printf("Error: Cierre del socket fallido.");
        return 1;
    }
    
    // Se libera memoria 
    free(op);
    free(nombreModuloAtencion);
    free(stringId);
    
    return 0;
}