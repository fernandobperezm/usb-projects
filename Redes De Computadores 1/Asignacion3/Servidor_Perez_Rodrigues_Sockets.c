/*
    Universidad Simón Bolívar.
    
    Descripción:Este archivo contiene el código fuente que maneja el servidor
    para la asignación 3.

    Autores:
        Br. Fernando Pérez, carné: 12-11152.
        Br. Leslie Rodrigues, carné: 10-10613.
 
    Última modificación: 27/06/2016.

*/

#include <stdio.h>      // Para funciones de entrada y salida estándar de C.
#include <sys/socket.h> // Para sockets.
#include <arpa/inet.h>  // Para sockets.
#include <stdlib.h>     // Librería estándar
#include <string.h>     // Para manejo de strings.
#include <unistd.h>     // Para el uso de write y close.
#include <fcntl.h>      // Para write, open y sus constantes.
#include <errno.h>      // Para errno
#include <limits.h>     // Para INT_MAX
#include <time.h>       // Para el manejo del tiempo para el sistema.

#define MAX_LONG_MENSAJE 255 // Variable de prepocesador para definir la long máxima del mensaje.

/**
 *  Contiene la información de un vehículo.
 * 
 *      @param estado Estado es un carácter que representa si el es vehículo está
 *                    en el estacionamiento con ‘0’, que no está con ‘1’. Y es 
 *                    inicializado con el valor nulo.
 *      @param idVehiculo Entero sin signo que corresponde el identificador del vehículo.
 *      @param monto Entero largo que almacena el monto a cancelar.
 *      @param tentrada Variable tipo struct tm, y almacena la fecha y hora de 
 *                      entrada del vehículo.
 *      @param tsalida: Variable tipo struct tm, y almacena la fecha y hora de 
 *                      salida del vehículo.
 * 
 */
struct mensaje{
    char estado; 
    unsigned int idVehiculo; // Código que identifica al vehículo
    long int monto; // Monto a cancelar
    struct tm tentrada; // Fecha y hora de entrada.
    struct tm tsalida; // Fecha y hora de salida.
};

/**
 * 
 * Mantiene la información de cada carro que está estacionado actualmente.
 * 
 *      @param puestos Arreglo que almacena elementos de tipo mensaje, posee 200
 *                     posiciones una para representar cada puesto del estacionamiento.
 *      @param proximoAsignar Entero que contiene el índice del arreglo donde 
 *                            se insertará el próximo elemento.
 * 
*/
struct estacionamiento{
    struct mensaje puestos[200];
    int proximoAsignar;
};



/**
 *   Funcion que busca la posicion de un vehiculo en el estacionamiento,
 *   y en caso de que no la encuentre retorna -1. 
 * 
 *   Si el parametro puestolibre es 0, se localiza la primera posicion 
 *   NO ocupada y se almacena en el campo 'proximoAsignar' de la
 *   estructura estacionamiento. 
 * 
 *   Esta funcion permite que la inserccion en el arreglo puestos de la estructura
 *   estacionamiento sea de complejidad constante al calular el indice de la proxima
 *   inserccion. Asimismo, la informacion de los vehiculos quedara almacena en la 
 *   posicion mas cercana al principio del arreglo que sea posible.
 * 
 * Adicionalmente, en caso el parametro
 *   'puestolibre' sea 0, se modifica la proxima posicion a asignar.
 *
 *      @param idVehiculo  Identificador del vehiculo a buscar.
 *      @param estacionamiento estructura que contiene la informacion de los vehiculos 
 *             estacionados
 *      @param puestolibre Entero que vale 0 sino se ha encontrado puesto 
 *                         en caso contrario su posicion.
 *   
 *
 *      @return Entero que toma el valor de -1 cuando el vehiculo no se encuentra en el 
 *              estacionamiento, y su posicion en caso contrario.
 *       
 *
 */
int buscarVehiculo(unsigned int idVehiculo, struct estacionamiento* est,int puestolibre){
    int i;
    int j; //contador auxiliar
    
    int encontrado;//-1 sino se ha encontrado el vehiculo en caso contrario su posicion 
    encontrado = -1;
    
    // la operacion es de entrada se necesita buscar el proximo puesto libre a asignar 
    if (puestolibre){
        
        for (i = 0;i < 200 ; i++){
        
            // Los puestos libres son aquellos que no han sido inicializados o
            // aquellos en los que un vehículo haya salido.
            if (est->puestos[i].estado == '\0' ||est->puestos[i].estado == '1'){
                est->proximoAsignar = i; // Se actualiza el indice donde se almacenara el vehiculo
                puestolibre = 1; // Se actualiza indicando que ya se encontro un puesto libre
                break;
            }
            //Si se encuentra el vehiculo
            if (est->puestos[i].idVehiculo == idVehiculo){
                encontrado = i; // Se actualiza guardando la posicion del vehiculo
                break;
            }
        }
    } else {  // la operacion es de salida, se inicializa el contador
        i = 0;
    }
    
    j = i;
    // Si no se ha encontrado el vehiculo
    if (encontrado == -1){
        // Se itera hasta encontrarlo o hasta que el indice sea 200
        for (i = j; i < 200 ; i++){
        
            if (est->puestos[i].idVehiculo == idVehiculo){
                return i;   
            }
        }
    } else { //si ya se encontró el vehiculo pero aun se necesita encontrar puesto libre
        // Si no se ha encontrado puesto libre
        if (puestolibre) {
            // Se itera hasta encontrarlo
            for (i = j;i < 200 ; i++){
                //Si se encuentra un puesto libre
                if (est->puestos[i].estado == 0 ||est->puestos[i].estado == '1'){
                    est->proximoAsignar = i; //Se actualiza el indice donde se almacenara el vehiculo
                    
                    break;
                }   
            }
        }
    }
    return encontrado;
}

int main(int argc,char *argv[]){
    
    // Declaración de variables ------------------------------------------------
    int udpSocket;                         // Guarda el File Descriptor del socket.
    struct sockaddr_in servAddr;           // Estructura para el manejo de la dirección del servidor.
    struct sockaddr_in clientAddr;         // Estructura para el manejo de la dirección de los clientes.
    unsigned int longMensajeCliente;       // Longitud de los mensajes enviados por el servidor.
    char bufferMensajes[MAX_LONG_MENSAJE]; // Buffer para el almacenado de los mensajes.
    unsigned short serverPort;             // Server Port. 
    int longMensajeRecibido;               // Variable que representa la longitud de los mensajes recibidos por los clientes.
    char *bitacoraEntrada;                 // Variable para almacenar las operaciones de entrada.
    char *bitacoraSalida;                  // Variable para almacenar las operaciones de salida.
    int i;                                 // Contador de propósito general.
    char *p;                               // Apuntador para verificación de errores.
    int posVehiculo;                       // Puesto de un vehículo en un momento particular.
    unsigned short puestosOcupados = 0;    // Variable que indica los puestos ocupados.
    struct estacionamiento est;            // Para mantener una estructura de estacionamiento.
    int largo;                             // Para medir el largo de los argumentos.
    int fd;                                // Variable para descriptores de archivo
    struct mensaje msj;                    // Estructura para el mensaje recibido por el socket.
    int diaActualI;                        // Almacena el día actual del año para la entrada.
    int diaActualO;                        // Almacena el día actual del año para la salida.
    struct tm *apuntadorTiempoE;           // Estructura para el manejo del tiempo de entrada.
    struct tm *apuntadorTiempoS;           // Estructura para el manejo del tiempo de salida.
    time_t tiempo;                         // Variable necesaria para medir el tiempo del sistema.

    //Inicializaciones.
    diaActualI = -1;
    diaActualO = -1;
    tiempo = time(0);
    
    // Función necesaria para localtime_r
    tzset(); 
    
    // Se inicializa la estructura de estacionamiento y mensaje.
    est.proximoAsignar = 0;
    memset(&est, 0, sizeof(est)); 
    memset(&msj, 0, sizeof(msj));
    
    // Vericación del número de argumentos
    if (argc != 7){
        fprintf(stderr,"Uso: sem_svr -l <puerto_sem_svr> -i <bitácora_entrada>\
 -o <bitácora_salida");
        return 1;
    }
    
    // Verificación de Flags repetidos
    if (argv[1] == argv[3] || argv[3] == argv[5] || argv[5] == argv[1]){
        fprintf(stderr, "Error: Existen Flags repetidos.\n");
        return 2;
    }
    
    //Para cada flag.

    for (i = 1; i<argc -1; i += 2 ){
        largo = strlen(argv[i]);
        /* Se verifica que todos los flags tengan el formato -Char */
        if(argv[i][0] != '-' || largo != 2){
            fprintf(stderr,"Error: %s no es un flag inválido.\n",argv[i]);
    		return 2;
        }
        /* Se ve cuál flag es el actual. */
        switch(argv[i][1]){
	        case 'l':
	            /* Manejo correcto de la conversión. */
                errno = 0; //Variable de errno.h para el manejo de errores.
                long conv = strtol(argv[i+1], &p, 10); // Se pasa el string, donde guardará el \0 y la base a convertir.
                
                // Chequeo de errores:
                //      El primero verifica que sea un entero, que haya habido una conversión.
                //      El segundo verifica se haya terminado correctamente con \0.
                //      El tercero verifica que el número no sea un puerto mayor a los posibles.
                if (errno != 0 || *p != '\0' || conv > 65536 || conv < 1024) {
                    //Aquí hay errores.
                    fprintf(stderr,"El número de puerto es inválido. El rango válido es de 1024 hasta 65536.\n");
                    return 3;
                } else {
                    // Aquí no hay errores.
                    serverPort = (unsigned short) conv; 
                }
		        break;
	        case 'i':
	            bitacoraEntrada = strdup(argv[i+1]);
		        break;
	        case 'o':
		        bitacoraSalida = strdup(argv[i+1]);
	    	    break;
	        default:
		        fprintf(stderr,"Error: %s no es un flag inválido.\n",argv[i]);
    		    return 2;
        }
    }
    
    // Se imprime el mensaje correspondiente al letrero
    printf("Estacionamiento con puestos.\n");
    
    /*Se añade la cabecera a las bitacoras indicando su formato*/
    
    //abrimos la bitacora de entrada
    fd = open(bitacoraEntrada, O_WRONLY|O_CREAT|O_APPEND,0600);
    if (fd==-1) {
        perror("Error al abrir fichero.");
        return 5;
    }
            
    //Escribimos en el archivo
    write(fd,"FORMATO DE LA BITACORA: \nHORA -- ID del vehículo\n\n",51);
  
    close(fd); //Se cierra el archivo
    
    //abrimos la bitacora de salida
    fd = open(bitacoraSalida, O_WRONLY|O_CREAT|O_APPEND,0600);
    // Si se produjo un error
    if (fd==-1) {
        perror("Error al abrir fichero.");
        return 5;
    }
            
    //Escribimos en el archivo
    write(fd,"FORMATO DE LA BITACORA: \nHORA -- ID del vehículo -- Monto a pagar\n\n",68);
    
    close(fd); //Se cierra el archivo
    
    /* Trabajo con el socket. */
    
    // Creación del socket -----------------------------------------------------
    if ((udpSocket = socket(PF_INET,SOCK_DGRAM, IPPROTO_UDP)) < 0){
        perror("El socket UDP no pudo ser creado.");
    }
    
    //Se construye la estructura local necesitada.
    memset(&servAddr, 0, sizeof(servAddr));         // Se inicializa la estructura en 0.
    servAddr.sin_family = AF_INET;                  // Familia de IPv4.
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);   // Cambia el formato a network byte order.
    servAddr.sin_port = htons(serverPort);          // Puerto local.
    
    // Se asocia el socket al puerto mencionado.
    if (bind(udpSocket, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
        perror("No se puede asociar el socket al puerto.");
    }
    
    // Ciclo infinito.
    while(1) {
        
        // Tamaño de el parámetro in-out.
        longMensajeCliente = sizeof(clientAddr);
        
        // Recepción ---------------------------------------------------------
        if ( (longMensajeRecibido = recvfrom(udpSocket, bufferMensajes, MAX_LONG_MENSAJE,
            0, (struct sockaddr *) &clientAddr, &longMensajeCliente) ) < 0){
            perror("Error al recibir el mensaje.");       
        }
        
        bufferMensajes[longMensajeRecibido] = '\0'; // Se indica el fin del mensaje.
        msj.estado = bufferMensajes[0];  // Se recibe si se quiere entrar o salir.
        msj.monto = 0.0; // Se indica el monto a pagar del vehículo.

        tiempo = time(0);
        
        switch(msj.estado){
            case '0':
                //Cuando se recibe '0' significa una entrada.
                if (puestosOcupados < 200) { 
                    char *idVehiculo = strdup(&(bufferMensajes[2])); // Se obtiene el Id del vehículo.
                    msj.idVehiculo = (unsigned int) atoi(idVehiculo); // Se convierte entero sin signo el ID del vehículo.
                    // Para verificación de duplicados, se busca en la estructura
                    // de estacionamiento el idVehiculo y se ve si no está 
                    // estacionado ya.
                    posVehiculo = buscarVehiculo(msj.idVehiculo, &est,1);
                    if ((posVehiculo != -1) && (est.puestos[posVehiculo].estado == '0')){
                        //Se guarda en el buffer el mensaje de respuesta al cliente.
                        //MENSAJE: byte de entrada, byte de puesto. 
                        bufferMensajes[0] = '0'; // La operación es de entrada.
                        bufferMensajes[1] = '2'; // El ID está repetido en el estacionamiento.
                        bufferMensajes[2] = '\0'; // Se termina el mensaje.
                        longMensajeCliente = 3;
                    } else{
                        puestosOcupados++; //Se aumenta la cantidad de vehículos en el estacionamiento. 
                        if (puestosOcupados == 200) {
                            printf("Se ha cambiado el mensaje de la pantalla:\n");
                            printf("Estacionamiento sin puestos.\n");
                        }
                        est.puestos[est.proximoAsignar] = msj; // Se guarda el vehículo en la memoria del estacionamiento.
                        //Se guarda en el buffer el mensaje de respuesta al cliente.
                        //MENSAJE: byte de entrada, byte de puesto, fecha, hora, identificador del vehiculo. 
                        bufferMensajes[0] = '0'; // La operación es de entrada.
                        bufferMensajes[1] = '0'; // Hay puestos en el estacionamiento.
                        apuntadorTiempoE = localtime_r(&tiempo,&(msj.tentrada)); // Se calcula la hora del sistema.
                        //Se añade la fecha al mensaje a enviar, se necesitan 18 bytes
                        //del mensaje para almacenar la fecha. El siguiente byte a 
                        //utilizar es el bufferMensajes[20].
                        strftime(&(bufferMensajes[2]),19,"%d/%m/%y %H:%M:%S ", &(msj.tentrada));
                        //Se copia el id del vehículo en el buffer, no incluye el \0.
                        unsigned long largo = strlen(idVehiculo);
                        for(i = 0;i < largo;i++){
                            bufferMensajes[20+i] = idVehiculo[i];
                        }
                        //Se añade el \0 para terminar el mensaje.
                        bufferMensajes[20+largo] = '\0';
                        longMensajeCliente = 20+largo+1;
                        
                        // Se escribe en la bitácora.
                        fd = open(bitacoraEntrada, O_WRONLY|O_CREAT|O_APPEND,0600);
                        if (fd==-1) {
                            perror("Error al abrir fichero.");
                            return 5;
                        }
                        
                        // Se escribe el membrete del día en la bitácora.
                        if (diaActualI != msj.tentrada.tm_mday){
                            diaActualI = msj.tentrada.tm_mday;// Se actualiza el día actual
                            write(fd,"Día ",4);
                            write(fd,&bufferMensajes[2],8); //se imprime la fecha
                            write(fd," ----------------------------\n",30);
                        }
                        
                        //Escribimos en el archivo
                        write(fd," ",1);
                        write(fd,&bufferMensajes[11],8);// se escribe la hora
                        write(fd," -- ",4); 
                        write(fd,idVehiculo,strlen(idVehiculo));
                        write(fd,"\n",1); 
                        
                        close(fd); //Se cierra el archivo
                    }
                    //Se hace free para no tener leaks de memoria.
                    free(idVehiculo);
                } else {
                    //Se guarda en el buffer el mensaje de respuesta al cliente.
                    //MENSAJE: byte de entrada, byte de puesto.
                    bufferMensajes[0] = '0'; // La operación es de entrada.
                    bufferMensajes[1] = '1'; // Hay puestos en el estacionamiento.
                    bufferMensajes[2] = '\0'; // Se termina el mensaje.
                    longMensajeCliente = 3;
                    //VERIFICAR SI HAY QUE GUARDARLO EN LA BITACORA TAMBIEN.
                }
                break;
            case '1': // cuando se recibe un '1' significa que es de salida
            
                // Se obtiene convierte entero sin signo el ID del vehículo.
                msj.idVehiculo = (unsigned int) atoi(&(bufferMensajes[1])); // Se obtiene el ID del vehículo.
                bufferMensajes[0] = '1'; // La operación es de salida.

                char *idVehiculo = strdup(&(bufferMensajes[1]));

                bufferMensajes[1] = '1'; //Indica que el vehiculo no esta en el estacionamiento  
                
                // Se obtiene la posición en el estacionamiento del vehículo, 
                // si éste está. Se verifica que no trate de salir un vehículo
                // que ya salió.
                posVehiculo = buscarVehiculo(msj.idVehiculo, &est,0);
                if ((posVehiculo == -1) ||( (posVehiculo != -1) && (est.puestos[posVehiculo].estado == '1'))){
                    //Se guarda en el buffer el mensaje de respuesta al cliente.
                    //MENSAJE: byte de entrada, byte de puesto. 
                    bufferMensajes[0] = '1'; // La operación es de salida.
                    bufferMensajes[1] = '2'; // El ID ya salió.
                    bufferMensajes[2] = '\0'; // Se termina el mensaje.
                    longMensajeCliente = 3;
                } else {
                    puestosOcupados--; // Se disminuye el contador de puestos
                    
                    //Si el estacionamiento ahora tiene puestos, se actualiza el letrero.
                    if (puestosOcupados == 199) {
                        printf("Se ha cambiado el mensaje de la pantalla:\n");
                        printf("Estacionamiento con puestos.\n");
                    }

                    bufferMensajes[1] = '0';//indica que el vehicul esta en el estacionamiento

                     //Se almacena fecha y hora actual
                    apuntadorTiempoS = localtime_r(&tiempo,&(est.puestos[posVehiculo].tsalida));
                    msj.tsalida = est.puestos[posVehiculo].tsalida;
                    
                    strftime(&(bufferMensajes[2]),20, "%d/%m/%y %H:%M:%S",&(est.puestos[posVehiculo].tsalida)); //Se almacena el string que contiene la fecha y la hora
                    
                    //Calculamos el monto a pagar
                    msj.monto = (long int) (80 + 30 *((long int)((long int)mktime(&msj.tsalida) - (long int)mktime(&msj.tentrada))/ 3600));
                    est.puestos[posVehiculo].monto = msj.monto;
                    
                    // añadimos el monto al msj
                    sprintf(&bufferMensajes[19],"%ld",msj.monto);
                    longMensajeCliente = strlen(bufferMensajes); //se determina el largo del msj
                    
                    est.puestos[posVehiculo].estado = '1'; // Se remueve el vehiculo, modificando su estado
                    
                    // Se escribe en la bitácora.
                    fd = open(bitacoraSalida, O_WRONLY|O_CREAT|O_APPEND,0600);
                    if (fd==-1) {
                        perror("Error al abrir fichero.");
                        return 5;
                    }
                    
                    // Se escribe el membrete del día en la bitácora.
                    if (diaActualO != msj.tsalida.tm_mday){
                        diaActualO = msj.tsalida.tm_mday;// Se actualiza el día actual
                        write(fd,"\nDía ",5);
                        write(fd,&bufferMensajes[2],8); //se imprime la fecha
                        write(fd," ----------------------------\n",30);
                    }
                    
                    //Escribimos en el archivo
                    write(fd," ",1);
                    write(fd,&bufferMensajes[11],8);// se escribe la hora
                    write(fd," -- ",4); 
                    write(fd,idVehiculo,strlen(idVehiculo));
                    write(fd," -- ",4); 
                    write(fd,&bufferMensajes[19],strlen(&bufferMensajes[19]));
                    write(fd,".00",3);
                    write(fd,"\n",1); 
                    
                    close(fd); //Se cierra el archivo
                }
                
                // Para que no hayan fugas de memoria.
                free(idVehiculo);
                break;
        }
        
        // Se envía el mensaje al cliente
        if (sendto(udpSocket,bufferMensajes, longMensajeCliente, 0, 
           (struct sockaddr *) &clientAddr, sizeof(clientAddr)) != longMensajeCliente){
            perror("Error. No se pudo enviar el mensaje.");
        }
    }
    
    //Salida del programa.
    free(bitacoraEntrada);
    free(bitacoraSalida);
    return 0;
}