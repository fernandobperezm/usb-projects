% Universidad Simón Bolívar.
% 
% Descripción: Este programa es el archivo principal del proyecto en PROLOG,
% de la implementación de árboles bien etiquetados como estructuras y como
% listas.
%
% Autores:
%   Br. Fernando Pérez, carné: 12-11152.
%   Br. Leslie Rodrigues, carné: 10-10613.
% 
% Última modificación: 16/06/2016.

/*
    Definición de árboles como estructuras.
    Para definir un árbol como una estructura, se manejan dos subestructuras:
    Nodo y Arista. Cada una tiene su respectiva etiqueta 
    
    Nodo: Los nodos son functores que contienen los siguientes argumentos:
        + EtiquetaN: Representa la etiqueta del nodo.
        + Lista: Representa la lista de aristas a los hijos del nodo.
    
    Arista: Representa la arista dirigida del nodo padre al nodo hijo. Es un
            functor que contiene los siguientes argumentos:
        + EtiquetaA: Representa la etiqueta de la arista, debe cumplir que
                   EtiquetaA = Abs(EtiqNodoPadre - EtiqNodoHijo)
        + Nodo: Representa a un nodo hijo, está compuesto por el functor definido 
              anteriormente.
*/
nodo(EtiquetaN,[]) :- integer(EtiquetaN), EtiquetaN  > 0.
nodo(EtiquetaN,[arista(_,_)|Z]) :- integer(EtiquetaN), nodo(EtiquetaN,Z). 

arista(EtiquetaA,nodo(_,_)) :- integer(EtiquetaA), EtiquetaA > 0.

/*
    bienEtiquetado es un predicado que toma un árbol como estructura como entrada y 
    se unifica correctamente si éste está bien etiquetado. Un etiquetamiento 
    correcto es aquel donde cada nodo y arista tiene una etiqueta única.
    
    Argumentos de entrada:
    + Nodo: Representa a un nodo del árbol. En el caso base, el nodo no tiene
            hijos y su etiqueta debe ser uno. En el caso recursivo, se utiliza
            un conjunto de predicados auxiliares para llamar al predicado
            bienEtiquetadoNodo que unifica correctamente si todos los nodos
            y aristas están bien etiquetados.
             
*/
bienEtiquetado(nodo(EtiquetaN,[])) :- 
    EtiquetaN =:= 1.
bienEtiquetado(nodo(EtiquetaN,[arista(EtiquetaA, nodo(EtiquetaN2,X))|Z])) :- 
    contarNodos(
        nodo(EtiquetaN,[arista(EtiquetaA, nodo(EtiquetaN2,X))|Z]),
        NroNodos
     ),
     NroAristas is NroNodos - 1,
     numlist(1, NroNodos, EtiqNodosVal),
     numlist(1, NroAristas, EtiqAristasVal),
     bienEtiquetadoNodo(
        nodo(EtiquetaN,[arista(EtiquetaA,nodo(EtiquetaN2,X))|Z]),
        EtiqNodosVal,
        EtiqAristasVal,
        [],
        [],
        _,
        _
     ).

/*  bienEtiquetadoNodo es un predicado auxiliar que determina si el nodo actual 
    bien etiquetado, para esto, verifica que el nodo esté en un conjunto válido de
    etiquetas para los nodos y recursivamente llama a bienEtiquetadoArista que
    se encarga de determinar si todas las aristas hijas están bien etiquetadas.
    
    Los argumentos de entrada son:
    + Nodo: Representa al nodo actual a verificar.
    + NodosVal: Es una lista que representa el conjunto de etiquetas de nodos válidas 
              para el nodo actual.
    + AristasVal: Es una lista que representa el conjunto de etiquetas de aristas válidas 
                para el nodo actual.
    - NVisF: Representa una lista que contiene los nodos visitados al momento
             de verificar el nodo. Se utiliza para los argumentos de bienEtiquetadoArista
    - AVisF: Representa una lista que contiene los aristas visitados al momento
             de verificar el nodo. Se utiliza para los argumentos de bienEtiquetadoArista
             
    El caso base representa las hojas del árbol y el caso recursivo a los nodos
    raíz e intermedios con hijos.
*/ 
bienEtiquetadoNodo(nodo(EN,[]),NodosVal,_,NodosVis,AristasVis,NVisF, AVisF) :-
    insertarElem(EN,NodosVis,NVisF),  
    member(EN,NodosVal),
    AVisF = AristasVis, !.
    
bienEtiquetadoNodo(nodo(EN,X),NodosVal,AristasVal,NodosVis,AristasVis,NVisF,AVisF) :- 
    insertarElem(EN,NodosVis,NodosVis2),  
    member(EN,NodosVal), 
    bienEtiquetadoArista(X,EN,NodosVal,AristasVal,NodosVis2,AristasVis,NVisFArista,AVisAristas),
    NVisF = NVisFArista,
    AVisF = AVisAristas.

/*  bienEtiquetadoArista es un predicado auxiliar que determina si la arista actual 
    está bien etiquetada, para esto, verifica que la etiqueta de la arista cumpla
    con EtiquetaA = Abs(EtiqNodoPadre - EtiqNodoHijo), además, añade la arista
    al conjunto de aristas visitadas, y verifica que la etiqueta sea válida,
    seguidamente, verifica que las demás aristas del mismo nodo estén bien etiquetadas
    y si lo están, verifica que el nodo hijo esté bien etiquetado.
    
    Los argumentos de entrada son:
    + Arista: Representa a la arista actual a verificar.
    + ENP: Representa la etiqueta del nodo padre.
    + NodosVal: Es una lista que representa el conjunto de etiquetas de nodos válidas.
    + AristasVal: Es una lista que representa el conjunto de etiquetas de aristas válidas 
                para la arista actual.
    + NodosVis: Representa una lista que contiene los nodos visitados al momento
                de verificar la arista.
    + AristasVis: Representa una lista que contiene los aristas visitados al momento
                  de verificar la arista.
    - NVisF: Representa una lista que contiene los nodos visitados al terminar
             de hacer todas las verificaciones con la arista actual
    - AVisF: Representa una lista que contiene los aristas visitados al al terminar
             de hacer todas las verificaciones con la arista actual
             
    El caso base representa el caso donde ya se verificaron todas las aristas del 
    nodo que llamó al predicado y el caso recursivo representa cada arista del mismo.
    raíz e intermedios con hijos.
*/ 
bienEtiquetadoArista([],_,_,_,NodosVis,AristasVis,NVisF,AVisF) :- 
    AVisF = AristasVis, 
    NVisF = NodosVis, !.
    
bienEtiquetadoArista([arista(EA,nodo(ENH,X))|Y],ENP,NodosVal,AristasVal,NodosVis,AristasVis,NVisF,AVisF) :-
    EA =:= abs(ENH-ENP),
    insertarElem(EA,AristasVis,AristasVis2),
    member(EA,AristasVal), 
    bienEtiquetadoArista(Y,ENP,NodosVal,AristasVal,NodosVis,AristasVis2,NVisFAux,AVisFAux),
    bienEtiquetadoNodo(nodo(ENH,X),NodosVal,AristasVal,NVisFAux,AVisFAux,NVisF,AVisF).

/*  contarNodos es un predicado auxiliar que recibe un árbol como estructura y 
    dependiendo si el primer argumento es un nodo o una arista realiza cálculos distintos.
    
    Los argumentos de entrada son:
    + Nodo o Arista: Representa a un nodo o una arista. 
                     Si es un nodo, debe calcular la cantidad de nodos que hay en 
                     los hijos y luego se suma él.
                     Si es una arista, hace el conteo de los nodos que hay a 
                     partir del nodo al que conecta, luego cuenta los nodos que 
                     hay en las demás aristas y luego suma estas cantidades.
    - NroNodos: Número de nodos resultante de calcular el número de nodos 
                hijos con el nodo actual.
    
    En el caso recursivo, representa una hoja o una lista vacía de aristas, en
    el primer caso unifica NroNodos en 1 y en el segundo en 0.
*/         
contarNodos(nodo(_,[]),NroNodos) :- NroNodos is 1. %Si es un nodo hoja.
contarNodos([],NroNodos) :- NroNodos is 0. %Si ya se revisaron todas las aristas.
contarNodos(nodo(_,X),NroNodos) :- 
    contarNodos(X,NroNodos2), !, NroNodos is NroNodos2 + 1. %Se obtienen los nodos de abajo y luego se suma el actual.
contarNodos([arista(_,X)|Y],NroNodos) :- 
    contarNodos(X,NroNodosX), contarNodos(Y,NroNodosY), !, NroNodos is NroNodosX + NroNodosY. %Se buscan los nodos de la actual arista, luego el de los demás, se suman luego.
/* 
   Predicado para insertar un elemento en un conjunto verificando que antes no
   esté en el mismo.
*/
insertarElem(Elemento,ConjuntoE,ConjuntoS) :- 
    notMember(Elemento,ConjuntoE), %Se ve que el elemento no esté en el conjunto.
    ord_add_element(ConjuntoE, Elemento, ConjuntoS). %Se añade finalmente al conjunto.

/* Predicado que determina si un elemento NO está en una lista. */    
notMember(_,[]) :- !.
notMember(E,[X|Tail]) :- dif(E,X), notMember(E,Tail).

% PARTE 2. Definición de árboles como listas.

/*
    Etiquetamiento es un predicado que dado un esqueleto (árbol como lista de listas)
    determina todos los árboles bien etiquetados asociados al mismo.
    
    Argumentos:
    + Esqueleto: Es un esqueleto de árbol.
    - Arbol: Es un árbol como estructura que está bien etiquetado.
    
    En el caso base, solamente se tiene un esqueleto donde la única lista no tiene
    hijos y ésta se representa como un nodo raíz sin hijos donde la única etiqueta
    posible es 1.
    
    En el caso recursivo, crea un árbol etiquetado con ceros, luego cuenta los nodos
    crea el conjunto de etiquetas de aristas y nodos válidas y utiliza el predicado
    auxiliar etiquetarYVerificar para unificar Arbol.
*/
etiquetamiento(esq([[0]]),Arbol) :-
    Arbol = nodo(1,[]). 

etiquetamiento(esq(X),Arbol) :- 
    crearArbol(X,ArbolSinEtiquetar),
    contarNodos(ArbolSinEtiquetar,NroNodos),
    NroAristas is NroNodos - 1,
    numlist(1, NroNodos, EtiqNodosVal),
    numlist(1, NroAristas, EtiqAristasVal), !,
    etiquetarYVerificar(ArbolSinEtiquetar,EtiqNodosVal,EtiqAristasVal,Arbol).

/*
    etiquetarYVerificar es un predicado auxiliar dado un árbol sin etiquetar 
    y genera todos los etiquetamientos válidos para esa misma configuración de árbol.
    
    Argumentos:
    + ArbolSinEtiquetar: Es un árbol como estructura con etiquetas en 0.
    + EtiqNodosVal: Representa un conjunto de etiquetas de nodos válidos.
    + EtiqAristasVal: Representa un conjunto de etiquetas de aristas válidas.
    - Arbol: Es un árbol como estructura que está bien etiquetado.
    
    Lo que hace el predicado es generar una lista de permutaciones de etiquetas
    válidas y pasa esa lista a etiquetarNodo como el conjunto de etiquetas de nodo
    válidas, en caso de que el árbol producido no esté bien etiquetado, determina
    otra permutación hasta que unifique correctamente.
*/    
etiquetarYVerificar(ArbolSinEtiquetar,EtiqNodosVal,EtiqAristasVal,Arbol) :-
    permutation(EtiqNodosVal,PermutacionENV),
    etiquetarNodo(ArbolSinEtiquetar,PermutacionENV,EtiqAristasVal,_,_,Arbol).

/*
    etiquetarNodo es un predicado que dado un árbol como estructura, lo etiqueta
    y a su vez verifica que el etiquetamiento sea correcto.
    
    Argumentos:
    + Arbol: Es un árbol sin etiquetar.
    + ENV: Representa el conjunto de etiquetas válidas de nodos al momento etiquetar el nodo.
    + EAV: Representa el conjunto de etiquetas válidas de aristas al momento etiquetar el nodo.
    - ENVS Representa el conjunto de etiquetas válidas de nodos al terminar de verificar las aristas
    - EAVS: Representa el conjunto de etiquetas válidas de aristas al al terminar de verificar las aristas
    - Arbol: Es un árbol como estructura que está bien etiquetado.
*/

etiquetarNodo(A,ENV,EAV,ENVS,EAVS,Arbol) :- 
    nodo(EN,Lista) = A,
    [EtiqN|EnvAux] = ENV,%random_member(EtiqN,ENV), %Se obtiene la primera etiqueta del conjunto válido.
    etiquetarArista(Lista,EtiqN,EnvAux,EAV,ENVS,EAVS,ListaAristas), %Se obtiene recursivamente todas las aristas con las nuevas etiquetas.
    cambiarEtiqueta(nodo(EN,ListaAristas),EtiqN,Arbol). %Se construye el árbol con la nueva etiqueta.

/*
    etiquetarArista es un predicado que dado una arista, lo etiqueta
    y a su vez verifica que el etiquetamiento sea correcto.
    
    Argumentos:
    + Arista: Es una arista sin etiquetar.
    + EtiqNodoPadre: Representa la etiqueta válida del nodo padre.
    + ENV: Representa el conjunto de etiquetas válidas de nodos al momento etiquetar la arista.
    + EAV: Representa el conjunto de etiquetas válidas de aristas al momento etiquetar la arista.
    - ENVS Representa el conjunto de etiquetas válidas de nodos al terminar de verificar las aristas
    - EAVS: Representa el conjunto de etiquetas válidas de aristas al al terminar de verificar las aristas
    - Aristas: Es una lista de aristas bien etiquetadas.
    
    En el caso base, unifica la lista de nodos y aristas visitados como la que recibe
    y la lista de aristas como la arista vacía. En el caso recursivo, descompone
    la lista de aristas en cabeza y cola, luego etiqueta la cabeza haciendo todas
    las verificaciones correspondientes y determina que las demás aristas estén bien 
    etiquetadas, como último, unifica Aristas como la lista que contiene a la arista
    actual bien etiquetada y la cola como la lista de demás aristas bien etiquetadas. 
*/    
etiquetarArista([],_,ENV,EAV,ENVS,EAVS,Aristas) :-
    ENVS = ENV,
    EAVS = EAV,
    Aristas = [].
    
etiquetarArista(A,EtiqNodoPadre,ENV,EAV,ENVS,EAVS,Aristas) :- 
    [arista(EA,nodo(ENH,X))|Y] = A,
    etiquetarNodo(B,ENV,EAV,ENVSAux,EAVSAux,Arbol),
    nodo(ENH,X) = B,
    nodo(EtiqNodoHijo,_) = Arbol,
    arista(EA,Arbol) = AristaAnterior,
    Etiq is abs(EtiqNodoHijo-EtiqNodoPadre), %Se calcula la etiqueta que debe tener la arista.
    member(Etiq,EAVSAux), %Se verifica que la arista sea válida.
    ord_del_element(EAVSAux,Etiq,EAVSAux2), %Se elimina del conjunto de etiquetas utilizables.
    cambiarEtiqueta(AristaAnterior,Etiq,AristaActual), %Se crea la nueva arista
    etiquetarArista(Y,EtiqNodoPadre,ENVSAux,EAVSAux2,ENVS,EAVS,Lista2),
    Aristas = [AristaActual|Lista2]. %Se devuelve la lista de aristas


/*
    cambiarEtiqueta recibe un nodo o una arista, y lo devuelve con su respectiva 
    etiqueta modificada.
    
    Entrada:
        + nodo o arista, donde lista y X son listas 
          de aristas de un nodo, ENH es la etiqueta del nodo hijo,
        + Etiqueta es su etiqueta asociada.
    Salida:
        - NodoSalida o Arista salida, es el nodo o la arista con la etiqueta modificada.
    
*/
cambiarEtiqueta(nodo(_,Lista),Etiqueta,NodoSalida) :-
    NodoSalida = nodo(Etiqueta,Lista).

cambiarEtiqueta(arista(_,nodo(ENH,X)),Etiqueta,AristaSalida) :-
    AristaSalida = arista(Etiqueta,nodo(ENH,X)).


/*
    crearArbol dado un esqueleto genera su arbol asociado con las etiquetas inicializadas en 0.

    Entrada:
        + Lista de lista de enteros, donde cada entero representa la cantidad de hijos de ese nodo.

    Salida:
        - Arbol: es la estructura del arbol con las etiquetas inicializadas en cero.

*/
crearArbol([_|[]],Arbol) :-
    Arbol = nodo(0,[]), !.
    
crearArbol([Raiz,Hijos|Resto],Arbol) :-
    crearAristas(Raiz,Hijos,Resto,ListaAristas),
    Arbol = nodo(0,ListaAristas), !.


/*
    crearAristas: dado un la lista de enteros correspondiente al nivel de un arbol, 
    genera una lista de aristas.
    
    Entrada:
        + Hijos la cabeza de la lista.
        + RestoNodos la cola de la lista.
    Salisa:
        - ListaAristas: la lista con las aristas formadas.

*/
crearAristas([0|_],_,_,ListaAristas) :-
    ListaAristas = [], !.

crearAristas([Elem1|Resto],Hijos,RestoNodos,ListaAristas) :-
    Elem is Elem1 - 1 , !,
    [_|RestoH] = Hijos,
    crearArbol([Hijos|RestoNodos],Arbol), %Crea el subárbol del hijo.
    AristaActual = arista(0,Arbol), %Crea la arista actual con el árbol creado anteriormente.
    crearAristas([Elem|Resto],RestoH,RestoNodos,Lista2), %Crea las otras aristas para la misma raíz.
    ListaAristas = [AristaActual|Lista2]. % Crea la lista de aristas de la raíz actual.



% PARTE 3.

%--------------------------------------------------------------------------------

/*
    imprimir es predicado que dado una lista de nodos y una de sus aristas 
    asociadas, correspondientes a un camino lo imprime.
    
    Entradas:
        + lista que contiene las etiquetas de los nodos donde Hnodos es 
          la cabeza de la lista y Tnodos la cola.
        + lista que contiene las etiquetas de las aristas donde Haristas es
          la cabeza de lista Taristas su cola.
          
*/
imprimir([Hnodos|[]],[]):-
	write("("),write(Hnodos),write(")."),nl.
	%write(Hnodos).
imprimir([Hnodos|Tnodos],[Haristas|[]]):-
	%write(Hnodos),
    write("("),write(Hnodos),write(")-"),write(Haristas),write("->"),
	imprimir(Tnodos,Taristas).

imprimir([Hnodos|Tnodos],[Haristas|Taristas]):-
	write("("),write(Hnodos),write(")-"),write(Haristas),write("->"),
        %write(Hnodos),
	imprimir(Tnodos,Taristas).



/*
    recorrerAux es un predicado que almacena en listas el camino desde cada hijo 
    de la raíz hasta cada hoja del árbol.
    
    Entrada:
        + un arbol representado por un nodo, en el cual EtiquetaN corresponde a la
          etiqueta del nodo, EtiquetaA  a la etiqueta de una arista y Z es 
          la cola de la lista que contiene las aristas de un nodo.
    
    Salida:
        - Lnodo : lista que almacena etiquetas de nodos
        - Larista: lista que almacena la etiqueta de las aristas
        
*/
recorrerAux(nodo(EtiquetaN,[]),Lnodo,Larista):-
	member(EtiquetaN,Lnodo),
	append(Lnodo,[EtiquetaN],L),!.

recorrerAux(nodo(EtiquetaN,[]),Lnodo,Larista):-
	append(Lnodo,[EtiquetaN],L1),
    write("Caminos desde la raiz a cada hoja."),nl,
    write("Las etiquetas de los nodos se encuentran entre parentesis. "),nl,
    write("Las etiquetas de las aristas se encuentran entre guiones medios."),nl
	imprimir(L1,Larista),!.

recorrerAux(nodo(EtiquetaN,[arista(EtiquetaA,Hijo)|Z]),Lnodo,Larista) :-
	member(EtiquetaN,Lnodo),
	append(Larista,[EtiquetaA],L2),
	recorrerAux(Hijo,Lnodo,L2),
	recorrerAux(nodo(EtiquetaN,Z),Lnodo,Larista).

recorrerAux(nodo(EtiquetaN,[arista(EtiquetaA,Hijo)]|[]),Lnodo,Larista) :-
	append(Lnodo,[EtiquetaN],L1),
	append(Larista,[EtiquetaA],L2),
	recorrerAux(Hijo,L1,L2).

recorrerAux(nodo(EtiquetaN,[arista(EtiquetaA,Hijo)|Z]),Lnodo,Larista) :-
	append(Lnodo,[EtiquetaN],L1),
	append(Larista,[EtiquetaA],L2),
	recorrerAux(Hijo,L1,L2),
	recorrerAux(nodo(EtiquetaN,Z),L1,Larista).

/*
    describirEtiquetamiento es un predicado que dado un arbol imprime en pantalla
    su etiquetamiento. Mostransdo en pantalla los camino desde la raíz hasta cada hoja.

    Entrada:
        + un arbol representado por un nodo, en el cual EtiquetaN corresponde a la
          etiqueta del nodo, EtiquetaA  a la etiqueta de una arista y Z es 
          la cola de la lista que contiene las aristas de un nodo.
*/

describirEtiquetamiento(nodo(EtiquetaN,[])):-
	write("("),write(EtiquetaN),write(")"),nl.
	
describirEtiquetamiento(nodo(EtiquetaN,[arista(EtiquetaA,Hijo)|Z])) :-
	recorrerAux(Hijo,[EtiquetaN],[EtiquetaA]),
	recorrerAux(nodo(EtiquetaN,Z),[EtiquetaN],[]).



%-------------------------------------------------------------------------------


/*
    l es un predicado que genera una lista de enteros que respresentan la cantidad 
    de hijos que tiene cada nodo.
    
    Entrada:
        + N: El número de nodos.
        + R: la aridad máxima actual
        + NroHijos: Cardinalidad de la lista a generar
        
    Salida:
        - [H|T]: Lista de enteros que representan el numero de hijos que tiene cada nodo
          en ese nivel. 
*/

l(N,R,0,[]):- !.

l(0,R,NroHijos,[H|T]):-
    NroHijos2 is NroHijos - 1,
    H is 0,
    l(0,R,NroHijos2,T),!.

l(N,R,NroHijos,[H|T]) :-
    NroHijos2 is NroHijos - 1,
    N - R > 0,
    H is R,
    M is N -R,
    l(M,R,NroHijos2,T).

l(N,R,NroHijos,[H|T]) :-
    NroHijos2 is NroHijos - 1,
    N > 0,
    N - R =< 0,
    H is 0,
    l(0,R,NroHijos2,T).

/*
    esqueleto  es un predicado  que dado el numero de nodos y el máximo
    numero de hijos, genera todos los esqueletos de arboles posibles.
    
    Entrada:
        + N: numero de nodos del arbol
        + R: aridad maxima del arbol, es decir, la cantidad maxima de hijos que
             puede tener cada nodo,
             
    Salida: 
        - esqueleto de un arbol
*/  



esqueleto(N,R,esq([H|T])) :-
    R > 0,
    N > 0,
    between(1,R,Num),
    Num < N,
    M is N - Num,
    H = [Num],
    auxEsqueleto(M,R,esq(T),H).


/*
    esqueletoAux  es un predicado  que dado el numero de nodos y el máximo
    numero de hijos, genera todos los esqueletos de arboles posibles sin la raíz.
    
    Entrada:
        + N: numero de nodos del arbol
        + R: aridad maxima del arbol, es decir, la cantidad maxima de hijos que
             puede tener cada nodo.
        + Acc: contiene la lista correspondiente al nivel anterior.
             
    Salida: 
        - esqueleto de un arbol.
*/ 

auxEsqueleto(1,R,esq([X]),Acc) :-
	sum_list(Acc,Suma),
	l(0,R,Suma,X),
	!.

auxEsqueleto(N,R,esq([H|T]),Acc) :-
    N > 0,
    between(1,R,Num),
    Num < N,
    sum_list(Acc,Suma2),
    l(N,Num,Suma2,H),
    sum_list(H,Suma),
    Acc2 is N - Suma,
    auxEsqueleto(Acc2,Num,esq(T),H).

%-------------------------------------------------------------------------------

/*
    esqEtiquetables es un predicado que dado el numero máximo de hijos y el numero de
    nodos, genera todos los esqueletos posibles, y es cierto cuando cada uno de estos
    pueden ser bien etiquetados.
    
    Entrada: 
        + N: numero de nodos del arbol
        + R: aridad maxima del arbol, es decir, la cantidad maxima de hijos que
             puede tener cada nodo.
*/

esqEtiquetables(R,N) :-
    forall(esqueleto(N,R,Esqueleto),etiquetamiento(Esqueleto,Arbol)).





