%parte 2


l(N,R,0,[]):- !.
l(0,R,NroHijos,[H|T]):-
    NroHijos2 is NroHijos - 1,
    H is 0,
    l(0,R,NroHijos2,T),!.
l(N,R,NroHijos,[H|T]) :-
    %M is N - 1,
    NroHijos2 is NroHijos - 1,
    %N > 0,
    N - R > 0,
    H is R,
    M is N -R,
    l(M,R,NroHijos2,T).
l(N,R,NroHijos,[H|T]) :-
    %M is N - 1,
    NroHijos2 is NroHijos - 1,
    N > 0,
    N - R =< 0,
    %N - NroHijos > 0,
    %NroNietos is N - R,
    H is 0,
    l(0,R,NroHijos2,T).

esqueleto(N,R,[H|T]) :-
    R > 0,
    N > 0,
    between(1,R,Num),
    Num < N,
    M is N - Num,
    H = [Num],
    auxEsqueleto(M,Num,T,H).


auxEsqueleto(1,R,[X],Acc) :-
	sum_list(Acc,Suma),
	l(0,R,Suma,X),
	!.
auxEsqueleto(N,R,[H|T],Acc) :-
    N > 0,
    between(1,R,Num),
    Num < N,
    l(N,Num,Acc,H),
    sum_list(H,Suma),
    Acc2 is N - Suma,
    auxEsqueleto(Acc2,Num,T,H).
    
    



/*
escoger(N,R,Num) :-
    N - R > 0,
    N - R =< R,
    Num is N - R,!.
escoger(N,R,Num) :-
    N - R > R,
    Num is R,!.
escoger(N,R,Num) :-
    N - R =< 0,
    Num is 0.
*/
/*
l(N,R,0,X).
l(N,R,NroHijos,[H|T]) :-
    M is N - 1,
    NroHijos2 is NroHijos - 1,
    N > 0,
    N - R >= R, 
    H is R,
    l(M,R,NroHijos2,T).
l(N,R,NroHijos,[H|T]) :-
    M is N - 1,
    NroHijos2 is NroHijos - 1,
    N > 0,
    N - R < R,
    NroNietos is N - R,
    H is NroNietos,
    l(M,R,NroHijos2,T).
l(N,R,NroHijos,[H|T]) :-
    M is N - 1,
    NroHijos2 is NroHijos - 1,
    N - R < 0,
    H is 0,
    l(M,R,NroHijos2,T).
l(N,R,NroHijos,[H|T]) :-
    M is N - 1,
    NroHijos2 is NroHijos - 1,
    N =< 0,
    H is 0,
    l(M,R,NroHijos2,T).

    
    
esqueleto(N,R,[H|T]) :- 
    M is N - 1,
    H is R,
    
    auxEsqueleto(M,R,T,H).


auxEsqueleto(0,R,X,Acc) :- X = [],!.    
auxEsqueleto(N,R,[H|T],Acc) :-
    
    %between(1,R,Num),
    l(N,R,Acc,H),
    auxEsqueleto(M,R,T,Acc).


/*
l(R,1,[R]) :- !.
l(R,Count,[H|T]) :- H is R,Count2 is Count - 1,  l(R,Count2,T), !.


esqueleto(N,R,[H|T]) :- 
    M is N -1,
    l(R,1,H),
    auxEsqueleto(M,R,1,T).
    
auxEsqueleto(1,R,Acc,[X]):- 
    l(0,(Acc + 1)*2,X).
auxEsqueleto(N,R,Acc,[H|T]):-
    M is N -1,
    Acc2 is Acc + 1,
    between(1,R,Num),
    auxEsqueleto(M,R,Acc2,T),
    l(R,(Acc2-1)*R,H).
    
r(X,N):-
    between(1,N,Temp),
    X is Temp + 10.*/*/