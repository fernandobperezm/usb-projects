%P01 - Find the last element of a list.
my_last(X,[X]).
my_last(X,[_|Z]) :- my_last(X,Z).

%P02 - Find the last but one element of a list.
not_so_last(X,[X,_|[]]).
not_so_last(X,[_|Z]) :- not_so_last(X,Z).

%P03 - Find the k'th element of a list.
element_at(X,[X|_],1).
element_at(X,[_|Z],K) :- K > 1, K1 is K -1, element_at(X,Z,K1).

%P04 - Find the number of elements of a list.
number_elems([],0).
number_elems([_|Z],X) :- number_elems(Z,X1), X is X1 + 1.

%P05 - Reverse a list.
reverse_list(L1,L2) :- rev(L1,L2,[]).

rev([],L2,L2) :- !.
rev([X|Xs],L2,Acc) :- rev(Xs,L2,[X|Acc]). 

%P06 - Check if a list is a palindrome.
is_palindrome(X) :- reverse_list(X,Y), X == Y.

%P07 -- Flatten a list.
my_flatten([],[]) :- !.

my_flatten([X|Xs],Y) :- 
    is_list(X), !,
    my_flatten(X,Y1),
    my_flatten(Xs,Y2),
    append(Y1,Y2,Y).
    
my_flatten([X|Xs],Y) :-
    my_flatten(Xs,Y1),
    append([X],Y1,Y).
    
%P08 - Eliminate consecuite duplicates of a list elements.
compress([],[]).

compress([X],[X]).

compress([X,X|Xs],Y) :-
    compress([X|Xs],Y).
    
compress([X,X2|Xs],[X|Y]) :-
    X \= X2, compress([X2|Xs],Y).
    
%P09 - Pack consecutive duplicates of list elements into sublists.
% pack(L1,L2) :- the list L2 is obtained from the list L1 by packing
%    repeated occurrences of elements into separate sublists.
%    (list,list) (+,?)

pack([],[]).
pack([X|Xs],[Z|Zs]) :- transfer(X,Xs,Ys,Z), pack(Ys,Zs).

% transfer(X,Xs,Ys,Z) Ys is the list that remains from the list Xs
%    when all leading copies of X are removed and transfered to Z

transfer(X,[],[],[X]).
transfer(X,[Y|Ys],[Y|Ys],[X]) :- X \= Y.
transfer(X,[X|Xs],Ys,[X|Zs]) :- transfer(X,Xs,Ys,Zs).

% 1.10 (*) Run-length encoding of a list.
% Use the result of problem 1.09 to implement the so-called run-length encoding 
% data compression method. Consecutive duplicates of elements are encoded as 
% terms [N,E] where N is the number of duplicates of the element E.

% Example:
% ?- encode([a,a,a,a,b,c,c,a,a,d,e,e,e,e],X).
% X = [[4,a],[1,b],[2,c],[2,a],[1,d][4,e]]

encode(List,X) :-
    pack(List,Y),
    count(Y,X).
    
count([],[]) :- !.

count([List1|Tail],[[NumElems,Elem]|Tail2]) :-
    [Elem|_] = List1,
    number_elems(List1,NumElems),
    count(Tail,Tail2).