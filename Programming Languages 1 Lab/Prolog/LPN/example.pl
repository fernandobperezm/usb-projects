% Este es un comentario.

% Primeras cosas en Prolog.
gusta(juan,vino).
gusta(juan,maria).
gusta(maria,vino).
gusta(maria,oro).
persona(juan).
persona(maria).

% Si se pone gusta(X,Y), persona(Y).
% el intérprete responde X = Juan, Y = Maria;
% false (en rojito)

% Si se pone gusta(X,Y), persona(Z).
% X = Z, Z = Juan, Y = maria;
% X = juan, Z = maria, Y = Z;
% X = Z, Z = juan, Y = vino;
% X = juan, Y = vino, Z = maria;
% X = maria, Y = vino, Z = juan;
% X = Z, Z = maria, Y = vino;
% X = maria, Y = oro, Z = juan...

% Segundas cosas en prolog.
humano(socrates).
ateniense(socrates).
ateniense(aristoteles).
griego(X) :- ateniense(X), humano(X).

% Si se pone griego(X) imprime:
% X = socrates;
% false.
% Esto pasa porque para que alguien sea griego debe ser ateniense y humano, 
% dado que solamente socrates cumple con estas condiciones, entonces el es el unico
% griego.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Para probar igualdad y diferencia entre predicados.
% se utiliza = para igualdad y \= para diferencia.

%Por ejemplo, si se pone:
% juan = juan.
% true.

% gusta(juan,X) = gusta(juan,maria).
% X = maria.

% gusta(juan,X) \= gusta(juan,maria).
% false.
% Esa proposición lo que le pregunta a Prolog si no puede realizar una instanciación
% Prolog encuentra una instanciacion y por eso devuelve false, si no pudiese instanciar
% también devuelve false.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Para especificar un o (o lógico).

europeo(X) :- griego(X).
europeo(X) :- humano(X).

% Para expresar estructuras.
%Una estructura que tome un producto o una suma de enteros y la evalúe.
eval(suma(X,Y),R) :- 
    eval(X,R1), eval(Y,R2), R is R1 + R2.
    
eval(producto(X,Y),R) :-
    eval(X,R1), eval(Y,R2), R is R1 * R2.
    
eval(X,X) :- integer(X).

%Estructura para el factorial.
fact(X,1) :- integer(X), X = 0.
fact(X,0) :- integer(X), X < 0.
fact(X,R) :- integer(X), X1 is X - 1, fact(X1,R1), R is R1*X.

%Estructura para un árbol binario.
% Esta es la definición del árbol: nodo(valor, Arbol, Arbol)

        





