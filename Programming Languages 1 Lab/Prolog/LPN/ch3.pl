%Chapter 3.
%Este capítulo define la recursión como instrumento para lograr unificar.

%Ejemplo 1.
% Un functor que define la digestión de un Y por un X. (X digiere a Y).
is_digesting(X,Y) :- just_ate(X,Y). %X hace la digestión de Y si se lo acaba de comer.
is_digesting(X,Y) :- 
    just_ate(X,Z), 
    is_digesting(Z,Y). %Si X se acaba de comer a Z y Z está haciendo la digestión de Y

%algunos átomos útiles para la BC.
just_ate(mosquito,blood(john)).
just_ate(frog,mosquito).
just_ate(stork,frog).


%Ejemplo 2.
%Relación descendente entre hijos.

%Se definen algunos hijos.
child(bridget, caroline). %Caroline es hija de Bridget.
child(caroline, donna). %Donna es hija de Caroline.

%Se añade la relación recursiva de descendencia.
descend(X,Y) :- child(X,Y). %Eres descendiente si eres hijo directo.
descend(X,Y) :- 
    child(X,Z), descend(Z,Y). %O tienes varios hijos de por medio.


%Ejemplo 3.
%Sucesores.
numeral(0).
numeral(succ(X)) :- numeral(X).

%Ejemplo 4.
%suma de sucesores.

%Manera del libro.
%add(0,Y,Y).
%add(succ(X),Y,succ(Z)) :- add(X,Y,Z).

%Manera mía.
add(0,Y,Z) :- Y = Z.
add(succ(X), Y,Z) :- add(X,Y,Z1), Z = succ(Z1).


%EJERCICIOS.
%Ejercicio 1.
descend2(X,Y) :- child(X,Y).
descend2(X,Y) :- descend2(X,Z), descend2(Z,Y).

%Esta implementación genera un problema de recursión infinita, dado que, 
% puede buscar siempre la descendencia por la izquierda de descendencias de Z.
% Pudiendo incluso no terminar.

%Ejercicio 2.
%Matryoshka dolls.
directlyIn(katarina,olga).
directlyIn(olga,natasha).
directlyIn(natasha,irina).

in(X,Y) :- directlyIn(X,Y).
in(X,Y) :- directlyIn(X,Z), in(Z,Y).


%Ejercicio 3.
%Trenes directos e indirectos.
directTrain(saarbruecken,dudweiler). 
directTrain(forbach,saarbruecken). 
directTrain(freyming,forbach). 
directTrain(stAvold,freyming). 
directTrain(fahlquemont,stAvold). 
directTrain(metz,fahlquemont). 
directTrain(nancy,metz).

travelFromTo(X,Y) :- directTrain(X,Y).
travelFromTo(X,Y) :- directTrain(X,Z), travelFromTo(Z,Y).

%Ejercicio 4.
%Comparación relacional de numerales.
greater_than(X,0).
greater_than(succ(X),succ(Y)) :- greater_than(X,Y).


%Ejercicio 5.
% árboles binarios y swap entre ellos.
 Exercise  3.5 Binary trees are trees where all internal nodes have exactly two children. The smallest binary trees consist of only one leaf node. We will represent leaf nodes as leaf(Label) . For instance, leaf(3) and leaf(7) are leaf nodes, and therefore small binary trees. Given two binary trees B1 and B2 we can combine them into one binary tree using the functor tree/2 as follows: tree(B1,B2) . So, from the leaves leaf(1) and leaf(2) we can build the binary tree tree(leaf(1),leaf(2)) . And from the binary trees tree(leaf(1),leaf(2)) and leaf(4) we can build the binary tree tree(tree(leaf(1),  leaf(2)),leaf(4)) .

Now, define a predicate swap/2 , which produces the mirror image of the binary tree that is its first argument. For example:

   ?-  swap(tree(tree(leaf(1),  leaf(2)),  leaf(4)),T). 
   T  =  tree(leaf(4),  tree(leaf(2),  leaf(1))). 
   yes
















