/*
El siguiente es un programa que toma un problema que se resuelve con recursión, 
se convierte a recursión de cola y luego a repetición.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//Factorial de un número
//Recursivo.
unsigned long long fact(unsigned long long n){
    if (n == 0){
        return 1;
    }
    return n * fact(n-1);
}

//Recursivo de cola.
unsigned long long factAux(unsigned long long n, unsigned long long acum){
    if (n == 0){
        return acum;
    }
    return factAux(n-1, n*acum);
}

unsigned long long fact2(unsigned long long n, unsigned long long acum){
    return factAux(n,1);
}

//Recursión iterativa
unsigned long long factIter(unsigned long long n,unsigned long long acum){
    unsigned long long temp;
    while (n != 0){
        temp = n;
        n= n-1;
        acum = temp*acum;
   }
    return acum;
}
 
//Probar el primero:
int main(){
    clock_t start;
    clock_t end;
    float s1;
    float s2;
    float s3;
    unsigned long long n = 65;
    
    start = clock();
    unsigned long long res1 = fact(n);
    end = clock();
    s1 = (float)(end- start) / CLOCKS_PER_SEC;
    
    start = clock();
    unsigned long long res2 = fact2(n,1);
    end = clock();
    s2 = (float)(end- start) / CLOCKS_PER_SEC;
    
    start = clock();
    unsigned long long res3 = factIter(n,1);
    end = clock();
    s3 = (float)(end- start) / CLOCKS_PER_SEC;
    
    printf("1-Tiempo: %f\n\t %llu \n", s1,res1);
    printf("2-Tiempo: %f\n\t %llu \n", s2,res2);
    printf("3-Tiempo: %f\n\t %llu \n", s3,res3);
    return 0;
}
