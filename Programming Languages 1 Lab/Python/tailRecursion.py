'''
El siguiente es un programa que toma un problema que se resuelve con recursión, 
se convierte a recursión de cola y luego a repetición.
'''

#Factorial de un número
#Recursivo.
def fact(n):
    if n == 0:
        return 1
    return n * fact(n-1)
    
#Recursivo de cola.
def fact2(n):
    return factAux(n,1)

def factAux(n, acum):
    if n == 0:
        return acum
    return factAux(n-1, n*acum)
    
#Recursión iterativa
def factIter(n,acum):
    while (n != 0):
        n,acum = n-1, n*acum
    return acum

#Fibonacci
#Recursivo
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    
    return fib(n-1) + fib(n-2)

#Recursivo de cola.
def fib2(n):
    return fibAux(n,1,0)
    
def fibAux(n,acum1, acum2):
    if n == 0:
        return acum2
    elif n == 1:
        return acum1
    
    return fibAux(n-1, acum1+acum2, acum1)   
    

#Revertir una lista
    
#Probar el primero:
'''
print(fact(100))
print(fact2(100))
print(factIter(10003,1))
'''
print(fib(30))
print(fib2(30))
