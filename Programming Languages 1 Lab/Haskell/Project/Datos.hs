{- Universidad Simón Bolívar.
   Departamento de COmputación y Tecnologías de la Información.
   Laboratorio de Lenguajes I.
   Profesor: Federico Flaviani.
   
   Descripción: Archivo donde se encuentran las definiciones de tipos de datos,
   sinónimos y typleclasses necesarios para el funcionamiento correcto del proyecto.
   Además, se encuentran las definiciones de los operadores, así como de las 
   variables.
   
   Estudiantes:
        Br. Fernando Pérez, carné: 12-11152.
        Br. Leslie Rodrigues, carné: 10-10613.
        
   Última Modificación: 21/05/2016.
-}

{-#LANGUAGE FlexibleInstances #-}


module Datos where

-- Definición recursiva de los términos.
-- Las posibles opciones son: Var, Booleanos, Negado, Y, O, Implicación, 
-- Equivalencia e Inequivalencia.
-- Además, deriva Eq que permite comparar con == dos términos.
data Term = 
      Var Char
    | OurBool Bool
    | Neg Term
    | Or Term Term 
    | And Term Term 
    | Implies Term Term 
    | Equiv Term Term
    | NotEquiv Term Term 
    deriving (Eq)

-- Tipo de datos que representa a las ecuaciones Term === Term.
data Equation = Equation Term Term 

-- Sinónimo utilizado para representar sustitución de un término por otro término.
type Sust =  (Term, Term)

-- Typeclass sustitución que representa las diferentes sustituciones que podemos
-- tener, esto es, sustitución de un término por otro, de dos términos por dos
-- términos o de tres términos por tres términos.
class Sustitucion t where
    sustituir :: Term -> t -> Term
    
-- Instancia de Sustitución para la sustitución de un término por otro.
instance Sustitucion Sust where
    sustituir (Var x) (t1, Var y) 
        | x == y =  t1
        | otherwise = (Var x)

-- Instancia de Sustitución para la sustitución de dos términos por otros dos.
instance Sustitucion (Term, Sust, Term) where
    sustituir (Var x) (t1, (t2, Var y), Var z)
        | x == y = t1
        | x == z = t2
        | otherwise = Var x

-- Instancia de Sustitución para la sustitución de tres términos por otros tres.        
instance Sustitucion (Term, Term, Sust, Term, Term) where
    sustituir (Var x) (t1, t2, (t3, Var y1), Var y2, Var y3)
        | x == y1 = t1
        | x == y2 = t2
        | x == y3 = t3
        | otherwise = Var x

-- Instancia de Show de para el tipo de dato Term.   
instance Show Term where
    show (Var x) = [x]
    show (OurBool x1)
        | x1 == True = "true"
        | x1 == False = "false"   
    show (Neg t) = "neg " ++ (show t)
    show (Or t1 t2) =  "(" ++ (show t1) ++ " \\/ " ++ (show t2) ++ ")"
    show (And t1 t2) = "(" ++ (show t1) ++ " /\\ " ++ (show t2) ++ ")"
    show (Implies t1 t2) = "(" ++ (show t1) ++ " ==> " ++ (show t2) ++ ")"
    show (Equiv t1 t2) = "(" ++ (show t1) ++ " <==> " ++ (show t2) ++ ")"
    show (NotEquiv t1 t2) = "(" ++ (show t1) ++ " !<==> " ++ (show t2) ++ ")"

-- Typleclass que permite tener una sustitución y convertirlo en String.     
class ShowSust t where
    showSust :: t -> [Char]

-- Instancia de ShowSust para mostrar la sustitución de un término por otro.    
instance ShowSust Sust where
    showSust (t1,Var y) = "(" ++ show t1 ++ " =: " ++ show (Var y) ++ ")" 

-- Instancia de ShowSust para mostrar la sustitución de dos términos.
instance ShowSust (Term, Sust, Term) where
    showSust (t1, (t2, Var y), Var z) = 
        "( " ++ show t1 ++ ", " ++ show t2 ++ " =: " ++ (show $ Var y) ++ ", " ++ (show $ Var z) ++" )"

-- Instancia de ShowSust para mostrar la sustitución de tres términos.        
instance ShowSust (Term, Term, Sust, Term, Term) where
     showSust (t1, t2,(t3, Var x), Var y, Var z) = 
        "( "++ show t1 ++ ", " ++ show t2 ++ ", " ++ show t3 ++ " =: " ++ (show $ Var x) ++ ", " ++ (show $ Var y) ++ ", "++ (show $ Var z) ++" )"

-- Instancia de Show para el tipo de dato Equation.
instance Show Equation where
    show (Equation t1 t2) = (show t1) ++ " === " ++ (show t2)
    
-- Definición del operador infijo =:  
infixl 1 =:
(=:) :: Term -> Term -> Sust
(=:) t1 a = (t1, a)

-- Definición de la negación.
neg :: Term -> Term
neg t1 = Neg t1

-- Definición del operador infijo Disjunción \/
infixl 8 \/
(\/) :: Term -> Term -> Term
(\/) t1 t2 = Or t1 t2

-- Definición del operador infijo Conjunción /\
infixl 8 /\
(/\) :: Term -> Term -> Term
(/\) t1 t2 = And t1 t2

-- Definición del operador infijo implicación ==>
infixr 5 ==>
(==>) :: Term -> Term -> Term
(==>) t1 t2 = Implies t1 t2

-- Definición del operador infijo <==>
infixl 2 <==>
(<==>) :: Term -> Term -> Term
(<==>) t1 t2 = Equiv t1 t2

-- Definición del operador infijo !<==>
infixl 2 !<==>
(!<==>) :: Term -> Term -> Term
(!<==>) t1 t2 = NotEquiv t1 t2

-- Definición del operador infijo ===
infixl 0 ===
(===) :: Term -> Term -> Equation
(===) t1 t2 = Equation t1 t2


-- Definción funciones constantes para representar la variables 
a :: Term
a = Var 'a'

b :: Term
b = Var 'b'

c :: Term
c = Var 'c'

d :: Term
d = Var 'd'

e :: Term
e = Var 'e'

f :: Term
f = Var 'f'

g :: Term
g = Var 'g'

h :: Term
h = Var 'h'

i :: Term
i = Var 'i'

j :: Term
j = Var 'j'

k :: Term
k = Var 'k'

l :: Term
l = Var 'l'

m :: Term
m = Var 'm'

n :: Term
n = Var 'n'

o :: Term
o = Var 'o'

p :: Term
p = Var 'p'

q :: Term
q = Var 'q'

r :: Term
r = Var 'r'

s :: Term
s = Var 's'

t :: Term
t = Var 't'

u :: Term
u = Var 'u'

v :: Term
v = Var 'v'

w :: Term
w = Var 'w'

x :: Term
x = Var 'x'

y :: Term
y = Var 'y'

z :: Term
z = Var 'z'

-- Definción funciones constantes para representar los Booleanos
true :: Term
true = OurBool True

false :: Term
false = OurBool False

