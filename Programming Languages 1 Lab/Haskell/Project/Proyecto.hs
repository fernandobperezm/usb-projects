{- Universidad Simón Bolívar.
   Departamento de COmputación y Tecnologías de la Información.
   Laboratorio de Lenguajes I.
   Profesor: Federico Flaviani.
   
   Descripción: Archivo principal del proyecto, en éste se encuentran las funciones
   necesarias para crear el sistema de pruebas.
   
   Estudiantes:
        Br. Fernando Pérez, carné: 12-11152.
        Br. Leslie Rodrigues, carné: 10-10613.
        
   Última Modificación: 21/05/2016.
-}

module Proyecto where 

import Theorems
import Datos
import Control.Monad

-- Tipo de dato Sust, que representa la sustucion de uno, dos o tres variables.        
sust :: (Sustitucion s) => Term -> s -> Term
sust (Var x) s = sustituir (Var x) s
sust (OurBool b) s = (OurBool b)
sust (Neg t) s = Neg (sust t s)
sust (Or t1 t2) s = Or (sust t1 s) (sust t2 s)
sust (And t1 t2) s = And (sust t1 s) (sust t2 s) 
sust (Implies t1 t2) s = Implies (sust t1 s) (sust t2 s) 
sust (Equiv t1 t2) s = Equiv (sust t1 s) (sust t2 s)
sust (NotEquiv t1 t2) s = NotEquiv (sust t1 s) (sust t2 s)

-- Función que efectua una sustitucion a ambos lados de una Equation 
instantiate :: (Sustitucion s, ShowSust s) => Equation -> s -> Equation
instantiate (Equation t1 t2) s = Equation (sust t1 s) (sust t2 s)

-- Función que aplica la regla de Leibniz  
leibniz :: Equation -> Term -> Term -> Equation
leibniz (Equation t1 t2) e (Var z) = 
    Equation (sust e (t1 =: (Var z))) (sust e (t2  =: (Var z))) 

-- Función que hace una inferencia aplicando la regla de leibniz    
infer :: (Sustitucion s, ShowSust s) => Float -> s -> Term -> Term -> Equation
infer n s (Var z) e = 
    leibniz (instantiate (prop n) s) e (Var z)

-- terml: Dada una ecuacion devuelve el termino a la izquierda de la equivalencia
terml :: Equation -> Term
terml (Equation t1 t2) = t1

-- termr: Dada una ecuacion devuelve el termino a la derecha de la equivalencia
termr :: Equation -> Term
termr (Equation t1 t2) = t2

-- Función que hace la deducción de un paso de una demostración    
step :: (Sustitucion s, ShowSust s) => Term -> Float -> s -> Term -> Term -> Term 
step t1 n s (Var z) e 
    | terml (infer n s (Var z) e) ==  t1 = termr (infer n s (Var z) e)
    | termr (infer n s (Var z) e) ==  t1 = terml (infer n s (Var z) e)  
    | otherwise = error "La regla no se puede ejecutar."

-- Función que muestra en pantalla el hint de una demostración y el termino optenido apartir del mismo
statement :: (Sustitucion s, ShowSust s) => Float -> [Char] -> s -> [Char] -> [Char] -> Term -> Term -> Term -> IO Term
statement num with s using lambda (Var z) e termino = 
    let
        hint = "=== <statement "++ show num ++ " with " ++ showSust s ++ " using lambda " ++ (show $ Var z) ++ " (" ++ show e ++")>" 
        termino2  = step termino num s (Var z) e
    in do
        putStrLn hint
        print termino2 
        return termino2 
        
with :: [Char]
with = "with"

using :: [Char]
using = "using"

lambda :: [Char]
lambda = "lambda"


-- Funcion que muestra en pantalla el inicio de una desmostración
proof :: Equation -> IO Term
proof teorema = do
    putStrLn $ "prooving " ++ show teorema ++ "\n"
    print $ terml teorema
    return $ terml teorema

-- Función que muestra en pantalla si una prueba se logro o no
done :: Equation -> Term -> IO ()
done teorema t2 
    | (termr teorema) == t2 =do 
        putStrLn "\nproof successful"
    | otherwise =  error "Prueba Fallida."

