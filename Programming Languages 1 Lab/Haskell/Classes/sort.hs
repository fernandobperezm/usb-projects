insertSort :: (Ord t) => [t] -> [t]
insertSort [] = []
insertSort (x:tail) = insert x (insertSort tail)

insert :: (Ord t) => t -> [t] -> [t]
insert x [] = [x]
insert x (y:tail)
	| x <= y = x:(y:tail)
	| otherwise = y:(insert x tail)

mergeSort :: (Ord t) => [t] -> [t]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort list = merge (mergeSort(mitad1 list)) (mergeSort(mitad2 list))

mitad1 :: [t] -> [t]
mitad1 list = take (length list `div` 2) list

mitad2 :: [t] -> [t]
mitad2 list = drop (length list `div` 2) list

merge :: (Ord t) => [t] -> [t] -> [t]
merge [] l = l
merge l [] = l
merge (x:tail1) (y:tail2) = if x <= y then x:(merge tail1 (y:tail2)) else y:(merge (x:tail1) tail2)