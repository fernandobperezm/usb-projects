-- Función map
mapear :: (a -> b) -> [a] -> [b]
mapear _ []     = []
mapear f (x:xs) = (f x) : mapear f xs

mapear' :: (a -> b) -> [a] -> [b]
mapear' f xs = go f xs []
    where
        go _ [] acc     = reverse acc
        go f (x:xs) acc = go f xs $ (f x):acc
        -- go f (x:xs) acc = go f xs $ acc ++ [f x] -- no reverse

mapear'' :: (a -> b) -> [a] -> [b]
mapear'' f xs = foldl (\x y -> (f y):x) [] xs
              -- foldr ((:).f) [] xs
              
-- Quicksort
quickS :: [Int] -> [Int]
quickS []      = []
quickS (x:xs) = quickS [y | y <- xs, y <= x] ++ [x] ++ quickS [y | y <- xs, y > x]

-- Factores primos.
primeFactors :: Int -> [Int]
primeFactors n = go n 2 []
    where
        go n p acc
            | n < 2     = acc
            | otherwise = if n `mod` p == 0 then go (n `div` p) p (p:acc) else go n (next p) acc
                where
                    next p = head $ [x | x <- [(p+1)..], null [y | y <- [2..(x-1)], x `mod` y == 0 ]]