import Data.List

{- Problema 1
solveMeFirst a b = a + b

main = do
    val1 <- readLn
    val2 <- readLn
    let sum = solveMeFirst val1 val2
    print sum
-} 

{- Problema 2
hello_world = do
    putStrLn "Hello World"
    
    
-- This part relates to Input/Output and can be used as it is. Do not modify this section
main = do
   hello_world
-}

{- Problema 3, imprimir Hello World N veces.

hello_worlds n 
    | n == 1 = putStrLn "Hello World" -- Complete this function
    | otherwise = do
        putStrLn "Hello World"
        hello_worlds (n-1)
        
-- This part is related to the Input/Output and can be used as it is
-- Do not modify it
main = do
   n <- readLn :: IO Int
   hello_worlds n
-}

{- Problema 4, dado un entero y una lista, devolver una lista con todos los ele
mentos donde cada uno aparezca tantas veces diga el entero secuencialmente.
ejemplo
2
[1,2,3,4] -> [1,1,2,2,3,3,4,4]

f :: Int -> [Int] -> [Int]
f n [] = []
f n (x:arr) = (replicate n x) ++ (f n arr)  -- Complete this function

-- This part handles the Input and Output and can be used as it is. Do not modify this part.
main :: IO ()
main = getContents >>=
       mapM_ print. (\(n:arr) -> f n arr). map read. words
-}

{- Problema 5, dado un número y una lista, devolver los elementos de la lista
menores que el número (<) 
f :: Int -> [Int] -> [Int]
f n arr = filter (< n) arr --Fill up this function

-- The Input/Output section. You do not need to change or modify this part
main = do 
    n <- readLn :: IO Int 
    inputdata <- getContents 
    let 
        numbers = map read (lines inputdata) :: [Int] 
    putStrLn . unlines $ (map show . f n) numbers
-}

{- Problema 6, dado una lista, devolver una lsita que contenga solamente los
elementos en las posiciones 1, 3, 5 ... empezando a contar en 0.
f :: [Int] -> [Int]
f lst = faux lst 0 -- Fill up this Function

faux :: [Int] -> Int -> [Int]
faux [] n = []
faux (x:xs) n 
    | even n = faux xs (n+1)
    | otherwise = x:(faux xs (n+1))

-- This part deals with the Input and Output and can be used as it is. Do not modify it.
main = do
   inputdata <- getContents
   mapM_ (putStrLn. show). f. map read. lines $ inputdata
   
-}

{- Problema 7, dado un entero, devolver una lista cualquiera que tenga largo del entero.
fn n = replicate n 1
-}

{-Problema 8, dado una lista, reversarla.
rev l = reverse l

-}

{- Problema 9, dado una lista de elementos, sumar los que son impares. 

f arr = foldl (+) 0 $ filter (\x -> odd x) arr -- Fill up this function body

-- This part handles the Input/Output and can be used as it is. Do not change or modify it.
main = do
   inputdata <- getContents
   putStrLn $ show $ f $ map (read :: String -> Int) $ lines inputdata

-}

{- Problema 10. Dado una lista, determinar la cantidad de elementos sin utilizar
length y funciones de ese estilo.

--Only fill up the blanks for the function named len
--Do not modify the structure of the template in any other way
len :: [a] -> Int
len lst = foldl (+) 0 [1 | x <- lst]

-}

{- Problema 11. Dada una lista, crear una nueva lista cuyos elementos sean el 
valor absoluto de la lista original.

-- Enter your code here. Read input from STDIN. Print output to STDOUT

f arr = map (abs) arr-- Complete this function here

-- This section handles the Input/Output and can be used as it is. Do not modify it.
main = do
   inputdata <- getContents
   mapM_ putStrLn $ map show $ f $ map (read :: String -> Int) $ lines inputdata
   
-}

{- Problema 12. Dado un número, calcular el e^x de ese numero utilizando la 
serie infinita 1 + x^2/2!... truncado para 10 elementos.

fact :: Integer -> Integer
fact 0 = 1
fact x = x * fact (x-1)

pow :: Double -> Integer -> Double
pow _ 0 = 1
pow x y = x * (pow x (y-1))

exponentiate :: Double -> Integer -> Double
exponentiate x 10 = 0
exponentiate x y = res + exponentiate (x) (y+1)
    where 
        res = (pow x y) / fromInteger(fact y)

solve :: Double -> Double
solve x = exponentiate x 0-- Insert your code here --

main :: IO ()
main = getContents >>= mapM_ print. map solve. map (read::String->Double). tail. words


-}

{- Problema 13. Calcular numéricamente la integral debajo de una curva y el sólido
de revolución al girarlo por el eje X. INCOMPLETO>

import Text.Printf (printf)

solveaux :: Int -> Int -> [Int] -> [Int] -> Double -> Int -> Double
solveaux l r a b c i 
    | suma == r = sum $ zipWith (*) (map (fromIntegral) a) (map (c^^) b)
    | otherwise = (sum $ zipWith (*) (map (fromIntegral) a) (map (c^^) b)) + (solveaux l r a b (c+0.001) (i+1))
    where 
        suma = fromIntegral(l) + c*fromIntegral(i)

-- This function should return a list [area, volume].
solve :: Int -> Int -> [Int] -> [Int] -> Double
solve l r a b = solveaux l r a b 0.0 0 --Complete this function--

--Input/Output.
main :: IO ()
main = getContents >>= mapM_ (printf "%.1f\n"). (\[a, b, [l, r]] -> solve l r a b). map (map read. words). lines

-}

{- Problema 14. Reducir la siguiente lambda expresión:
((λx.(x y))(λz.z))
Respuesta: y
-}

{- Problema 15. Reducir la siguiente lambda expresión:
((λx.((λy.(x y))x))(λz.w))
Respuesta: w
-}

{- Problema 16. Reducir la siguiente lambda expresión:
((λx.(x x))(λx.(x x))) 
Respuesta: CAN'T REDUCE
-}

{- Problema 17. Reducir la siguiente lambda expresión:
(λg.((λf.((λx.(f (x x)))(λx.(f (x x))))) g)) 
Respuesta: CAN'T REDUCE
-}

{- Problema 18. Evalúe la siguiente lambda expresión:
(λx.x+1)3
Respuesta: 4
-}

{- Problema 19. Evalúe la siguiente lambda expresión:
(λx.x+1)((λy.y+2)3).
Respuesta: 6
-}

{- Problema 20. Evalúe la siguiente lambda expresión:
λx.λy.x^47y (recordar que los naturales se representanc como x(x(x(...xy)))
Respuesta: 47.
-}

{- Problema 21. Evalúe la siguiente lambda expresión:
λx.λy.x(xy). (recordar que los naturales se representanc como x(x(x(...xy)))
Respuesta: 2.
-}

{- Problema 22. Evalúe la siguiente lambda expresión:
λx.λy.y. (recordar que los naturales se representanc como x(x(x(...xy)))
Respuesta: 0.
-}

{- Problema 23. Ver si una lista es una función. 
buscarRepetidos :: [Int] -> Bool
buscarRepetidos (x:[]) = True
buscarRepetidos (x:y:resto) 
    | x == y = False
    | otherwise = buscarRepetidos(y:resto)

isFunction :: [(Int,Int)] -> Bool
isFunction list = if unique then True else False
    where 
        unique = buscarRepetidos $ sort [fst x | x <- list]

FALTA IO.
-}

