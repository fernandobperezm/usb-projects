-- Programa que calcula el máximo de una lista.
maximum' :: (Ord a) => [a] -> a
maximum' [] = error "La lista está vacía."
maximum' [x] = x
maximum' (x:xs)
    | x > maximum' xs = x
    | otherwise = maximum' xs
    
-- Usando funciones de haskell
maximum'' :: (Ord a) => [a] -> a
maximum'' [] = error "La lista está vacía."
maximum'' [x] = x
maximum'' (x:xs) = max x (maximum'' xs)

-- Haciendo replicate.
replicate' :: (Integral a) => a -> b -> [b]
replicate' a b 
    | a <= 0 = []
    | otherwise = b : replicate' (a-1) b
    
-- Reimplementando take
take' :: (Integral a) => a -> [b] -> [b]
take' _ [] = []
take' a (x:xs)
    | a <= 0 = []
    | otherwise = x: take' (a-1) xs
    
-- Reimplementando reverse
reverse' :: [a] -> [a]
reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x]

-- Hacemos repeat, produce una lista infinita
repeat' :: a -> [a]
repeat' a = a: repeat' a

-- Hacemos zip
zip' :: [a] -> [b] -> [(a,b)]  
zip' _ [] = []  
zip' [] _ = []  
zip' (x:xs) (y:ys) = (x,y):zip' xs ys  

-- Hacemos elem
elem' :: (Eq a) => a -> [a] -> Bool
elem' _ [] = False
elem' a (x:xs)
    | a == x = True
    | otherwise = elem' a xs
    
-- Ahora implementemos quicksort.
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y <= x] ++ [x] ++ quicksort [y | y <- xs, y > x]