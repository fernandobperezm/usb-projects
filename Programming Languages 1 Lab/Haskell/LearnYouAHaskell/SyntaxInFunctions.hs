{- En haskell se puede hacer que las funciones verifiquen ciertos patrones -}
lucky :: (Integral a) => a -> String
lucky 7 = "Lucky number seven!"
lucky x = "This is not a seven!"

{- El match se hace de manera descendiente, si en algún momento hay un match en 
el patrón, entonces se ejecuta ese cuerpo de la función, inclusive, se pueden
hacer más complicados, para que tengan patrones más complicados o con más casos-}
sayMe :: (Integral a) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"  
sayMe 4 = "Four!"  
sayMe 5 = "Five!"  
sayMe x = "Not between 1 and 5"  -- Caso general, este toma todos los valores posibles.

sayMe' :: (Integral a) => a -> String
sayMe' 1 = "One!" -- Este sayMe solamente funcionará cuando el argumento sea 1.
                  -- En otro caso, el compilador dirá Non-exhaustive patterns in function sayMe'
                  -- Indicando que hay un caso (o varios) a los que el no hace match.
                  
-- El match es interesante porque te permite definir funciones recursivamente 
-- tales como.
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)


-- Ejemplo de cuando no se cubren todos los casos con el pattern-matching.
charName :: Char -> String  
charName 'a' = "Albert"  
charName 'b' = "Broseph"  
charName 'c' = "Cecil"  -- Al ingresar charName 'd', nos dará el siguiente error:
                        -- Non-exhaustive patterns in function charName .
                        -- Por eso siempre es recomendable poner un caso genérico
                        -- que cubra todos los casos.
                        
-- Este es una función que no usa pattern matching..
addVectors :: (Num a) => (a,a) -> (a,a) -> (a,a)
addVectors a b = (fst a + fst b, snd a + snd b)

-- Este es la misma función haciendo uso de pattern matching.
addVectors' :: (Num a) => (a,a) -> (a,a) -> (a,a)
addVectors' (x1,y1) (x2,y2) = (x1 + x2, y1 + y2)

-- También, se puede hacer pattern-matching con listas y tuplas, con todo se puede hacer :D
-- Si se va a usar el patrón :, hay que tener en consideración que se hará match
-- cuando la lista sea de tamaño 1 o más.
head' :: [a] ->a
head' [] = error "Una lista vacía no tiene ni pies ni cabeza."
head' (x:_) = x

-- También, se pueden usar los as patterns, que tienen la siguiente sintaxis:
-- xs@(x:y:ys) y esto permite usar xs, sabiendo que xs cumple con ese patrón.
capital :: String -> String
capital "" = "Jeje, está vacío"
capital all@(x:xs) = "La primera letra de " ++ all ++ "es: " ++ [x]

-- Nota: No se puede usar ++ para hacer patrones.

{- Algo síper útil son las guardias. Funcionan de manera descendente al igual 
que el pattern-matching, además, funcionan sobre las variables de entrada
y normalmente saon escritas de la siguiente manera -}

bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "EMO."
    | bmi <= 25.0 = "Normal."
    | bmi <= 30.0 = "Gordo."
    | otherwise = "vacaaaaa." -- El caso otherwise siempre evalúa en True.
    
-- No solamente funciona para un argumento.
bmiTell' :: (RealFloat a) => a -> a -> String  
bmiTell' weight height  
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"  
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"  
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"  
    | otherwise                 = "You're a whale, congratulations!"  

-- Sin embargo, se repiten mucho las palabras weight y height, usemos la cláusula where.
bmiTell'' :: (RealFloat a) => a -> a -> String
bmiTell'' weight height
    | bmi <= skinny = "You are skinny."
    | bmi <= normal = "You're normal."
    | bmi <= fat    = "You are fat."
    | otherwise     = "You're an orc."
    where bmi = weight / height ^ 2
          skinny = 18.5
          normal = 25.0
          fat    = 30.0
 
-- Usando guardias podemos crear una nueva versión de max.
max' :: (Ord a) => a -> a -> a
max' a b 
    | a > b     = a
    | otherwise = b
    
{- También se pueden usar cases. y su sintaxis es la siguiente. 
        case expression of pattern -> result
                           pattern -> result
                           pattern -> result
                           ...
-}

--Redefiniendo head.
head'' :: [a] -> a
head'' xs = case xs of []    -> error "No se puede man."
                       (x:_) -> x
                       
-- La diferencia, es que los cases pueden usarse en cualquier lado (son expresiones)
-- en cambio, el pattern-matching solamente funciona en declaracion de funciones.
describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty"
                                               [x] -> "a singleton"
                                               xs -> "a longer list"