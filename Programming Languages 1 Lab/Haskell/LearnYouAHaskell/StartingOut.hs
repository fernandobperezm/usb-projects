{- 

Comentario multilínea.

-}
-- Para cambiar el nombre del intérprete.
--      :set prompt "ghci> "
-- Usar la palabra let en el intérprete es lo mismo declarar la función y cargar el archivo.

-- caracteres: 'a', 'b' y así.
-- Strings: "esto es un string" y es una lista de caracteres.
-- Operadores aritméticos: + - * / (-)
    -- Nota: el - unario debe estar así: (-5)
-- Operadores booleanos: && || not
    -- Nota: True y False
--Operadores Relacionales: == /= 

-- Funciones en infijo: Muy raras, solo algunas, por ejemplo, booleanos.
-- Funciones en prefijo: Más comunes, es lo que se asume. Se escriben: nombreFunción arg1 arg2 arg3 ... argn
    -- Algunas funciones aburridas: succ arg; min arg1 arg2; max arg1 arg2;
    -- Las funciones tienen la máxima precedencia, esto es: succ 9 + max 5 4 + 1 es lo mismo que (succ 9) + (max 5 4) + 1
    -- Las funciones con dos parámetros también pueden ponerse como infijo: div 92 10 es lo mismo que 92 `div` 10
    -- En Haskell no se necesitan paréntesis para escribir los argumentos de las funciones, esto es
        -- que succ (succ 3) significa que se aplica primero succ con argumento 3 y el resultado de éste es 
        -- evaluado por succ de afuera. Es lo equivalente en C a bar(bar(3))
       
-- Cómo declarar funciones: NombreFunción arg1 arg2 ... argn = X (lo que hace la función).
-- El nombre de las funciones NO PUEDE comenzar con mayúscula.
doubleUs x y = x*2  + y*2
doubleUs2 x y  = doubleMe x + doubleMe y -- Se pueden definir funciones a partir de otras ya creadas. No importa el orden de creación.
doubleMe x = x + x
multiply x y = x * y * y

-- Los if en haskell son del estilo if then else SIEMPRE. Y las instrucciones del then y del else deben ser expresiones
doubleSmallNumber x = if x > 100 then x else x*2

-- Se pueden escribir en líneas separadas para mejor entendimiento sin importar la identación.
doubleSmallNumber2 x = if x > 100
                        then x
                       else
                        x*2
-- Es válido el uso de ' en los nombres de las funciones. Normalmente denota cambios pequeños en las funciones ya creadas.
-- El if tiene más precedencia que las operaciones, pero menos que las funciones.
doubleSmallNumber' x = (if x > 100 then x else x*2) + 1
doubleSmallNumber'' x = if x > 100 then x else x*2 + 1

-- Las funciones también pueden definirse sin argumentos. Se le llaman definiciones o nombres.
conanO'Brien = "It's a-me, Conan O' Brien!"

-- Las listas en haskell almacenan elementos del MISMO TIPO.
lista1 = [1,2,3]

-- Las listas se pueden concatenar
lista2 = [1,2,3,4] ++ [9,10,11,12]
lista3 = "hello" ++ " " ++ "world"
lista4 = ['w','o'] ++ ['o','t']

{- El operador ++ tiene que recorrer todo el operando izquierdo, esto es ineficiente
    si la lsita del lado izquierdo es grande. Sin embargo, para concatenar cosas
    al principio de las listas podemos usar el operador : (operador cons)
    Este operador toma un número y una lista de números o un caracter y una lista de caracteres.
    En cambio, ++ toma dos listas.
-}
lista5 = 'A' : "Small cat"
lista6 = 5:[1,2,3,4,5]

-- Con el operador ++ sería:
lista5' = ['A'] ++ "Small cat"
lista6' = [5] ++ [1,2,3,4,5]

{- Para obtener la posición de un elemento en una lsita se usa el operador !! -}
pos1 = [1,2,3,4,5] !! 3 -- Resultado es 4.

-- Las listas pueden ser anidadas. Las listas internas deben ser TODAS del mismo tipo.
anidado = [[1,2,3,4],[5,3,3,3],[1,2,2,3,4],[1,2,3]]

-- Se pueden comparar listas. Se comparan en orden lexicográfico. Los operadores son < <= > >= == /=
comp1 = [3,2,1] > [5]
comp2 = ['A'] <= ['B']
comp3 = [3,2,1] == [3,2,1]
comp4 = [3] /= [2,1]

{-Funciones básicas con listas: head, tail, last, init, length, null, reverse, take, drop, maximum, minimum, sum, product, elem
    head [5,4,3] -> 5
    tail [5,4,3] -> [4,3]
    last [5,4,3] -> 3
    init [5,4,3] -> [5,4]
    length [5,4,3] -> 3
    null [] -> True; null [1,2,3] -> False
    reverse [5,4,3] -> [3,4,5]
    take 2 [5,4,3,2] -> [5,4]
    drop 3 [5,4,3,2,1] -> [2,1]
    maximum [5,4,3] -> 5
    minimum [5,4,3] -> 3
    sum [5,4,3] -> 12
    product [5,4,3] -> 60
    4 `elem` [ 3,4,5,6] -> True; 10 `elem` [ 3,4,5,6] -> False
-}

--Las listas pueden usar usar rangos para simplificación.
enum1 = [1..20]
enum2 = ['A'..'z']

--También puede decirse el paso al poner el primer elemento, el segundo y luego .. final.
enum3 = [2,4..20]
enum4 = [21,18..3]
enum5 = [0.1, 0.3 ..1] --Los números flotantes también joden aquí D: y cosas malas pasan.

-- Se pueden hacer listas infinitas y trabajar sobre ellas.
enum6 = take 24 [ 13,26..]

-- Cycle toma una lista y la repite infinitamente.
enum7 = take 10 (cycle [1,2,3])
enum8 = take 12 (cycle "LOL ")

-- repeat toma un elemento y hace una lista infinita de él.
enum9 = take 10 (repeat 5)

-- replicate es una opción más simple de hacer repeat.
enum10 = replicate 10 5

-- List comprehension
lc1 = [x*2 | x <- [1..10]]

-- Se pueden añadir predicados.
lc2 = [x*2 | x <- [1..10], x*2 >= 12]

lc3 = [x | x <- [1..100], mod x 7 == 3]

-- Uno más complicado
boomBangs xs = [if x <10 then "BOOM!" else "BANG!" | x <- xs, odd x] -- La función odd dice si un número es impar a través de True.

-- Se pueden añadir más predicados.
lc4 = [x | x <- [10..20], x /= 13, x /= 15, x /= 19]

-- Se puede hacer list comprehension con más de una variable
lc5 = [x*y | x <- [2,5,10], y <- [8,10,11]]

--Y también se pueden poner condiciones sobre esas listas.
lc6 = [x*y | x <- [2,5,10], y <- [8,10,11], x*y > 50]

-- Una nueva versión de length
length' xs = sum [1 | _ <- xs] -- _ indica que no importa lo que se obtenga, siempre se obtendrá de la lista.

-- Recordando que los strings son listas, podemos operar sobre ellos.
removeNonUppercase st = [c | c <- st, elem c ['A'..'Z']]

-- También se puede hacer list comprehension sobre listas anidadas.
lc7 xxs = [[x | x <- xs, even x ] | xs <- xxs]


{- Las n-tuplas también están presentes en haskell. Son útiles porque puedes guardar
    diferentes tipos de datos en una misma tupla, sin embargo, el tamaño de la tupla
    la hace de diferente tipo. Esto es, (1,2) es diferente de (1,2,3) para haskell
    
    Algunas operaciones interesantes para 2-tuplas son:
        - fst tupla1 -> devuelve el elemento en la posición 0 de la tupla.
        - snd tupla2 -> devuelve el elemento en la posición 1 de la tupla.
        
    Una función súper útil para n-tuplas es:
        - zip lista1 lista2 -> devuelve el producto cartesiano del elemento i de lista1 
                                con el elemento i de lista2 en lista de tuplas. 
                                El largo de la lista será el min (length lista 1) (length lista 2)
        
-}

triangles = [(a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10] ]
rightTriangles = [(a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10], a^2 + b^2 == c ^2 ]
perimeter24 = [(a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10], a^2 + b^2 == c ^2, a+b+c == 24]