{- Las funciones en Haskell están currificadas, esto quiere decir que
        - max 4 5 
        - (max 4) 5
        
   Viendo el tipo de max se puede ver que:
        - max :: (Ord a) => a -> a -> a que es lo mismo que
        - max :: (Ord a) => a -> (a -> a) 
   
   Es decir, que max se ve como una función que recibe un a y retorna una función
   que recibe un a y retorna otro a. Esto permite generar funciones que están
   parcialmente aplicadas si los argumentos no son pasados de manera completa.
-}

--Ejemplo de funciones currificadas
multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

{-Esto nos permite escribir en el intérprete las siguientes funciones que utilizan
la función anterior parcialmente aplicada:
    let multTwoWithNine = multThree 9
    multTwoWithNine 2 3 
        - Esto arroja 54.
    
    let multWithEighteen = multTwoWithNine 2
    multWithEighteen 10
        - Esto arroja 180.
-}

--Otro ejemplo de funciones currificadas.
compareWithHundred :: (Num a, Ord a) => a -> Ordering
compareWithHundred x = compare 100 x

-- Es lo mismo que:
compareWithHundred' :: (Num a, Ord a) => a -> Ordering
compareWithHundred' = compare 100

-- También podemos utilizar funciones infijas poniéndolas entre paréntesis.
-- Hacer divideByTen 200 es lo mismo que 200 / 10
divideByTen :: (Floating a) => a -> a
divideByTen = (/10)

isUpperAlphanum :: Char -> Bool
isUpperAlphanum = (`elem` ['A'..'Z'])

-- Si las funciones no son instancias de Show, entonces no se puede hacer esto:
    -- prelude> multThree 3 4

-- Función que recibe una función y aplica dos veces esa función a algo.
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)

-- Se reimplementa zipWith
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = (f x y): zipWith' f xs ys

-- Se reimplementa flip
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f x y = f y x

{- La función map recibe una función y una lista y devuelve una lista donde
   a cada elemento de la lista se le aplicó la función, es decir:
       map :: (a -> b) -> [a] -> [b]
       map _ [] = []
       map f (x:xs) = f x : map f xs

   Normalmente, escribir la función usando map es más legible que usando otras
   cosas, como por ejemplo: 
       map (+3) [1,2,3,4,5]
   Es  lo mismo que:
       [x+3 | x <- [1,2,3,4,5]]
       
-}

{- La función filter toma un predicado y una lista y devuelve una lista con 
   los elementos de la lista que cumplen con el predicado:
        filter :: (a -> Bool) -> [a] -> [a]
        filter _ [] = []
        filter f (x:xs)
            | f x = x:filter f xs
            | otherwise = filter f xs
            
    Ejemplos: 
        filter (>3) [1,2,3,4,5,6,7,8,9,0,0,9,8,7,6,5,4,3,2,1]
            [4,5,6,7,8,9,9,8,7,6,5,4]
        filter (`elem` ['A'..'Z']) "i lauGh At You BecAuse u r aLL the Same"  
            "GAYBALLS"  
-}

-- Reimplementamos quicksort utilizando filters.
quicksort' :: (Ord a) => [a] -> [a]
quicksort' [] = []
quicksort' (x:xs) = quicksort' (filter (<= x) xs ) ++
                    [x] ++
                    quicksort' (filter (> x) xs)
                    
-- Función que busca el número más grande menor que 100.000 y que es divisible 
-- entre 3829
largestDivisible :: (Integral a) => a
largestDivisible = head (filter f [100000,99999..])
    where f x = mod x 3829 == 0
    
{- La función takeWhile recibe un predicado y una lista y devuelve una lista 
de los elementos que cumplen con ese predicado hasta el primer elemento que evalúa
en false. Ejemplo:
    takeWhile (<3) [1,2,3,4,5,4,3,2,1]
        [1,2]
-}
-- La suma de todos los cuadrados impares que son menores a 10000.
oddSquares :: (Integral a) => a
oddSquares = sum( takeWhile (<10000) (filter odd (map (^2) [1..]))) 

-- Las cadenas de Collatz, si un número es par, se divide entre 2, sino, se 
-- multiplica por 3 y se le suma 1. La cadena termina cuando se alcanza 1.
collatz :: (Integral a) => a -> [a]
collatz 1 = [1]
collatz x
    | even x = x:collatz (x `div` 2)
    | odd x = x:collatz (x*3 + 1)
    
-- Tomamos las cadenas cuyo largo sea mayor a 15 en el conjunto [1..100]
numberOfCollatz :: Int
numberOfCollatz =  length (filter p (map (collatz) [1,2..100]))
    where p x = (length x) > 15
    
-- También se puede usar map de manera currificada.
listOfFuns :: [Integer -> Integer]
listOfFuns = map (*) [0..]


{- Los lambdas son los mismos que el lambda de lambda cálculo, la sintaxis es:
    \xs -> ... xs ...
-}

-- Se reimplementa la función numberOfCollatz.
numberOfCollatz' :: Int
numberOfCollatz' = length (filter (\xs -> length xs > 15) (map collatz [1..100]))


{- También se puede hacer pattern matching con lambdas, sin embargo, hay que 
tener cuidado porque si el pattern matching falla se genera un runtime error:
    map (\(a,b) -> a + b) [(1,2),(2,3),(4,5)]
        [3,8,9,8,7]
-}

-- Dos maneras de sumar tres números.
addThree :: (Num a) => a -> a -> a -> a
addThree x y z = x + y + z

addThree' :: (Num a) => a -> a -> a -> a
addThree' = \x -> \y -> \z -> x + y + z

-- Se puede reimplementar flip utilizando lambdas.
flip'' :: (a -> b -> c) -> (b -> a -> c)
flip'' f = \x -> \y -> f y x

{- 
    Los folds son funciones que encapsulan el típico pattern matching en donde se
    tiene una lista vacía y luego una lista del estilo (x:xs). 
    
    Los folds toman una función binaria, un acumulador (valor inicial) y una lista
    a la cual le van a hacer fold. La operación binaria toma el acumulador y el primer
    (o último) ekemento de la lista y produce un nuevo acumulador, y así se va
    operando hasta que se termine la lista, el último acumulador es el resultado
    de nuestro fold.
    
    Folds can be used to implement any function where you traverse a list once, 
    element by element, and then return something based on that. Whenever you 
    want to traverse a list to return something, chances are you want a fold.
    
    Tipos de fold:
        - foldl: empieza la operación de fold de izquierda a derecha, la operación
                 binaria es aplicada a la cabeza de la lista, se produce un nuevo
                 acumulador y se aplica al siguiente elemento, y así.
        
        - foldr: Funciona de manera similar al foldl, sin embargo, empieza a hacer
                 fold sobre el último de la lista y lo que recibe es una función
                 binaria, el elemento actual y luego el acumulador. Diferencia 
                 significativa: foldr trabaja con listas infinitas y foldl NO.

        - foldl1 y foldr1: Trabajan de una manera similar que el foldl y foldr, 
                           respectivamente, sin embargo, asumen que el contador
                           inicial es el primer elemento de la lista. Sin embargo,
                           esto requiere explícitamente que la lista no pueda ser
                           vacía.
                           
    Los scans trabajan de manera similar a los fold, sin embargo, éstos devuelven
    una lista que contiene todos los acumuladores utilizados. Existen también 4
    tipos de scan: scanl, scanr, scanl1, scanr1 que son todos equivalentes a 
    foldl, foldr, foldl1, foldr1, respectivamente.
    
    Ejemplos:
    ghci> scanl (+) 0 [3,5,2,1]  
        [0,3,8,10,11]  
    ghci> scanr (+) 0 [3,5,2,1]  
        [11,8,3,1,0]  
    ghci> scanl1 (\acc x -> if x > acc then x else acc) [3,4,5,3,7,9,2,1]  
        [3,4,5,5,7,9,9,9]  
    ghci> scanl (flip (:)) [] [3,2,1]  
        [[],[3],[2,3],[1,2,3]] 
-}

-- Implementación de suma con foldl.
sum' :: (Num a) => [a] -> a
sum' xs = foldl (+) 0 xs

-- Implementación utilizando lambdas.
sum'' :: (Num a) => [a] -> a
sum'' xs = foldl (\acc x -> acc + x) 0 xs

-- Sabiendo que podemos usar funciones parcialmente aplicadas.
sum''' :: (Num a) => [a] -> a
sum''' = foldl (\x acc -> acc + x) 0

-- Implementando elem con folds.
elem' :: (Eq a) => a -> [a] -> Bool
elem' x xs = foldl (\acc y -> if y == x then True else acc ) False xs

-- Implementando map con foldr.
map' :: (a -> b) -> [a] -> [b]
map' f xs = foldr (\x acc -> (f x):acc) [] xs

-- Funciones estándar con folds.
maximum' :: (Ord a) => [a] -> a
maximum' xs = foldr1 (\x acc -> if (x > acc) then x else acc) xs

reverse' :: [a] -> [a]
reverse' xs = foldl (\acc x -> x:acc) [] xs

product' :: (Num a) => [a] -> a
product' = foldl (\acc x -> x*acc ) 1 

filter' :: (a -> Bool) -> [a] -> [a]
filter' f xs = foldr (\x acc -> if (f x) then x:acc else acc) [] xs

head' :: [a] -> a
head' xs = foldr1 (\x _ -> x) xs

last' :: [a] -> a
last' = foldl1 (\_ x -> x) 


-- Uso de scanl, cuántos números son necesarios para que la suma de todos
-- sus raíces cuadradas sea mayor que 1000
sqrtSums :: Int  
sqrtSums = length (takeWhile (<1000) (scanl1 (+) (map sqrt [1..]))) + 1 


 










   
   
   
   
   
   
