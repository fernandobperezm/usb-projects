{- Para saber el tipo de una expresión en el intérprete es suficiente con poner:
    :t expr
   
   :: significa que una expresión tiene tipo de algo, por ejemplo:
    True :: Bool -> significa que True tiene tipo Bool
    
   Es buena práctica escribir el tipo de la función al momento de su definición:
    removeNonUppercase :: [Char] -> [Char]
    removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']
    
   Para una función que tenga varios argumentos:
    addThree :: Int -> Int -> Int -> Int
    addThree x y z = x + y + z -> Todos los argumentos y salidas se separan por ->
    
-}

{-  Tipos en Haskell:
        Int: Son los enteros, pero tienen un máximo y un mínimo: en 32 bits representan 
            desde -2147483648 hasta 2147483647
        
        Integer: Representan a los enteros también, pero, no tienen máximo ni mínimo
                sin embargo, los Int son más eficientes.
                
        Float: Son los punto flotantes con precisión simple (32 bits).
        
        Double: Son los punto flotantes con precisión doble (64 bits).
        
        Bool: True y False
        
        Char: Caracteres, [Char] es una lista de caracteres o String.
-}

{-  Type Variables: Son tipos genéricos, expresan que las funciones son de cualquier tipo.
                    Por ejemplo: 
                    1-  :t head
                        head :: [a] -> a
                    2-  :t fst
                        fst :: (a,b) -> a
-}

{- Typeclasses 101: Son interfaces que describen un comportamiento, es decir, 
                    si un tipo pertenece a una typeclass éste soporta e implementa
                    los comportamientos que son definidos por la segunda. NO SON CLASES,
                    más bien, son parecidos a las interfaces de Java, solo que mejores.
                    Por ejemplo:
                        :t (==)
                        (==) :: (Eq a) => a -> a -> Bool  
                            Esto se lee como: el operador == obtiene dos argumentos
                            del mismo tipo y retorna un booleano, para esto, se
                            aplican class constraint que es: (Eq a) que indica 
                            que el tipo de ambos argumentos debe pertenecer al mismo
                            class que Eq (Eq es un typeclass).
                            
                    Según el ejemplo, Eq es una interfaz que describe la verificación
                    de igualdad y cualquier tipo que quiera verificar igualdad ddebería
                    ser miembro de Eq. Todos los tipos de Haskell, exceptuando IO,
                    son miembros de Eq.
                    
    Algunos Typeclasses básicos:
        Eq: Usado para typos que soportan verificación de igualdad. Esto es, que
            las funciones y sus miembros implementan == y  /= y éstos aparecen
            en la definición del tipo.
            
        Ord: Para tipos que tienen orden. Para ser parte de Ord debes ser parte de Eq primero.
        
        Show: Los miembros de este typeclass pueden ser presentados como Strings.
              La función más usada de esta clase es show que toma una expresión
              parte de Show e imprime el String correspondiente.
              
        Read: Es lo contrario a Show, sin embargo, no son mutuamente excluyentes.
              La función más usada de esta clase es read que toma un string y 
              retorna el tipo correspondiente al miembro de Read.
                
              NOTA: Cuando se hace read str, si el string puede representar a más 
                    de un tipo, el compilador intentará hacer una inferencia de tipos,
                    si no puede inferir el tipo, lanzará un error. Para corregir esto,
                    también se pueden usar type annotations de este estilo:
                        read "5" :: Int
        
        Enum: Los miembros de Enum pueden ser ordenados secuencialmente, es decir,
              enumerados. Una ventaja importante de estar en Enum es que se pueden 
              usar en rangos de listas (operador ..) todos los elementos tienen 
              definidas las funcones succ y pred.
              Types in this class: (), Bool, Char, Ordering, Int, Integer, Float and Double.
              
        Bounded: Tienen mínimo y máximo. Tipos en esta clase: Int, Char, Bool.
        
        Num: Tipos numéricos, sus miembros se comportan como números. Los tipos
             numéricos son: Int, Integer, Float y Double.
             
        Integral y Floating: Son los enteros nada mas y los flotantes nada más, 
                             respectivamente, Int e Integer y Float y Double.
                             
            Nota: Función súper útil -> fromIntegral <- Toma un miembro de Integral 
                  y lo convierte en un Num.